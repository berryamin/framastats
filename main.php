<?php

/*
 *
 * Check arguments and launch dispatcher.php
 * with user arguments
 *
 * */

// ################## INCLUDE #####################
$path_Util		= "scripts/utils/Util.php";
include ($path_Util);

// ############### UTILS & DEBUG ##################
$util = new Util();
$util->setDebug("error");

// ################## CONSTANTS ###################
$nbOfArguments		= 2;
$nbOfArgumentsDebug	= $nbOfArguments + 1;

$arrayDatabases_allowed	= array ('mySQL', 'logsApache', 'socialNetworks', 'piwik', 'rest_json', 'bash_commands');

$path_pathToWebService 	= "./config/pathWebForServices.json";
$path_urlService 	= "./config/urlsForServices.json";
$path_dBService 	= "./config/databaseForServices.json";

$error			= "### ERROR ###";
$man			= "You should call script as follows :" . PHP_EOL .
			  "> php main.php framaXXX (debug || init)" . PHP_EOL . 
			  "framaXXX : \t" . $util->getAllKeysFromAnArray($util->checkJSONAndReturnArray($path_urlService));
$scriptEnds 		= "### SCRIPT ENDS ###" ;

// ################## VARIABLES ###################
$userChoices = array (
	'service' => '',
	'pathToWeb' => '',
	'database' => array(),
);

// ############## SPECIFIC FUNCTIONS ###############
/*
 * check if arguments passed to script are ok with our rules
 * @_arrayDatabases : the array with the names of databases we handle
 * return "OK" or an error Message
 * */
function checkArguments($_arrayDatabases, $_path_urlService, $_path_pathToWebService, $_path_dBService, $_serviceName, $_mode)
{
	global $util;

	$flagError = false;
	$message = "";

	// Handle service and url
	$stringToDefine = "The url of the service as framaservice.org/";
	if (!$util->checkValueIsRecorded_OrSetIt($_path_urlService, $_serviceName, $stringToDefine, null, $_mode) == "OK") {
		$flagError = true;
		$message = "### Problem with the service you ask" . PHP_EOL;
	}

	// Handle path to web
	$stringToDefine = "The path to the web folder";
	if (!$util->checkValueIsRecorded_OrSetIt($_path_pathToWebService, $_serviceName, $stringToDefine, null, $_mode) == "OK") {
		$message .= "### Problem with the service you ask" . PHP_EOL;
	}

	// Handle database
	$stringToDefine = "The database use for this service";
	if (!$util->checkValueIsRecorded_OrSetIt($_path_dBService, $_serviceName, $stringToDefine, $_arrayDatabases, $_mode) == "OK") {
		$message .= "### Problem with the service you ask" . PHP_EOL;
	}

	return ($flagError) ? $message : "OK";

}

// ################# MAIN SCRIPT ###################

$mode = null;

// Check number of arguments
if ($argc < $nbOfArguments || $argc > $nbOfArgumentsDebug) {
	$util->out($error . PHP_EOL . $man . PHP_EOL . $scriptEnds, "error", true); // exit
} 
// Debug And Init Mode
elseif ($argc == $nbOfArgumentsDebug) {
	$mode = $argv[$nbOfArgumentsDebug-1];
	if ($mode == "debug" || $mode == "init") {
		$util->setDebug("all");
		$modeUpperCase =  mb_strtoupper($mode);
		$util->out("### $modeUpperCase MODE", "info");
	} else {
		$util->out($error . PHP_EOL . $man . PHP_EOL . $scriptEnds, "error", true); // exit
	}
}

// Check validity of arguments
$res = checkArguments($arrayDatabases_allowed, $path_urlService, $path_pathToWebService, $path_dBService, $argv[1], $mode);

// End if init mode
if ($mode == "init") {
	$util->out("### END OF INIT MODE : you could add a module ($path_dBService), write your module or/and call php main.php $argv[1] (debug) ", "info", true); // exit
}

if ($res != "OK" ) {
	$util->out($man . PHP_EOL . $scriptEnds, "error");
}
else {
	// Arguments are ok
	$util->out("### START OF SCRIPT ###", "success");

	// Find and record choices
	$userChoices['service'] 	= $argv[1];
	$userChoices['pathToWeb'] 	= $util->getValueWithKey_inJSONFile($path_pathToWebService, $userChoices['service']);
	$userChoices['database'] 	= $util->getValuesWithKey_inJSONFile($path_dBService, $userChoices['service']);
	
	// Check if database is allowed (useful when we manually add in config file) 
	$stringToDefine = "The database use for this service";
	$util->checkValuesInArray ($userChoices['database'], $arrayDatabases_allowed, $path_dBService);

	// Display choices
	$util->out("### service \t\t\t: " . $userChoices['service']);
	$util->out("### path to web \t\t: " . $userChoices['pathToWeb']);
	$util->out("### databases \t\t\t: " . $util->array_toString($userChoices['database']));

	// Call script that will dispatchs according to our needs
	include('scripts/dispatcher.php');

}


?>
