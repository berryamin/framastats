<!DOCTYPE html>
<html lang="fr">

<head>
	<meta charset="utf-8">
	<title>Framastats</title>

	<!-- Styles -->
	<link href="styles/style_index.css" rel="stylesheet" />
	<link rel="shortcut icon" href="https://framasoft.org/nav/img/favicon.png">

	<!-- Jquery and Bootstrap and Framanav-->
	<script src="https://framasoft.org/nav/lib/jquery/jquery.min.js" type="text/javascript"></script>
	<script src="https://framasoft.org/nav/lib/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

	<script src="scripts/js/script_index.js" type="text/javascript"></script>
	<script src="scripts/js/script_index_Chart.js" type="text/javascript"></script>
	<script src="lib/Chart.js/Chart.js" type="text/javascript"></script>

	<?php /* Choose language */ include('scripts/php/selectLanguage.php');?>

</head>

<body data-spy="scroll" data-target="#myScrollspy">
<script src="https://framasoft.org/nav/nav.js" type="text/javascript"></script>

<!-- Retrieve All stats with AJAX call, then deploy with JS -->
<span id='getStats' style='display:none'></span>
<!-- /Retrieve All stats -->

	<main>
	<!-- Bootstrap grid : 2 - 10 -->
	<div class="container" id="Framasoft">
		<div class="row">

			<!-- Framastats-nav -->
			<div class="col-md-2" id="myScrollspy" role="navigation">
				<ul class="nav nav-tabs nav-stacked visible-md visible-lg" data-spy="affix" data-offset-top="45">
					<li class="active"><a href="#Framasoft"><?php echo TXT_FRAMASOFT;?></a></li>
					<li><a href="#Framadate">Framadate</a></li>
					<li><a href="#Framapad">Framapad</a></li>
					<li><a href="#Peertube">Peertube</a></li>
					<li><a href="#Framapic">Framapic</a></li>
					<li><a href="#MyFrama">MyFrama</a></li>
					<li><a href="#Framalibre">Framalibre</a></li>
					<li><a href="#Framablog">Framablog</a></li>
					<li><a href="#Framabook">Framabook</a></li>
					<li><a href="#Framasphere">Framasphere</a></li>
					<li><a href="#Framabin">Framabin</a></li>
					<li><a href="#Framaslides">Framaslides</a></li>
					<li><a href="#Framapiaf">Framapiaf</a></li>
<li><a href='#Framanews'>Framanews</a></li> 
<li><a href='#Framanotes'>Framanotes</a></li> 
<li><a href='#Framemo'>Framemo</a></li> 
<li><a href='#Framacarte'>Framacarte</a></li> 
<!-- NEXT LINK --> 
					<li><a href="#ReseauxSociaux"><?php echo TXT_SOCIAL_NETWORKS;?></a></li>
					<li><a href="#AutresStats"><?php echo TXT_OTHER_STATS ;?></a></li>
				</ul>
			</div>
			<!-- /Framastats-nav -->

			<div class="col-md-10">
				<!-- All stats --> <!-- Use a pattern like the Framadate div (not Framasoft div) to add more stats -->

				<!-- Framasoft -->
				<div class="divsCenter first">
					<div class="row serviceNameRow">
						<div class="col-md-8 col-xs-12">
							<h2>
							<span class="glyphicon glyphicon-tree-deciduous" aria-hidden="true"></span>
							<?php echo TXT_FRAMASOFT_NETWORK;?></h2>
						</div>
						<!-- Large devices -->
						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 
							<h6 class="textUpdate_large"><?php echo TXT_UPDATE;?><span class="statToFill framastats_lastUpdate"></span> </h6> 
						</div>
						<!-- Small devices -->
						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 
							<h6><?php echo TXT_UPDATE;?><span class="statToFill framastats_lastUpdate"></span></h6> 
						</div>
					</div>
					<h6><?php echo TXT_FRAMASOFT_DESCRIPTION;?> <a href="http://degooglisons-internet.org/" target="_blank"> <?php echo TXT_FRAMASOFT_DESCRIPTION_LIEN; ?>.</a> </h6>
					<div class="centerStats">
						<div class="panel-group">
							<!-- 1st Stat Framasoft -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseOne_framasoft" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framastats_years"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle">
														<?php echo TXT_FRAMASOFT_YEARS ;?>
													</div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseOne_framasoft" > <!-- add 'in' if we want to be open at start-->
									<div class="panel-body">
										<p><?php echo TXT_FRAMASOFT_YEARS_ORG_1; ?><strong><span class="statToFill framastats_years_organization"></span></strong><?php echo TXT_FRAMASOFT_YEARS_ORG_2 ;?></p>
									</div>
								</div>
							</div>
							<!-- 2nd Stat Framasoft -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="row">
											<div class="col-md-3 col-xs-2">
												<div class="statToFill statInTitle framasoft_countEmbeddedServices"><i class="fa fa-spinner" aria-hidden="true"></i></div>
											</div>
											<div class="col-md-8 col-xs-8">
												<div class="statNameInTitle"><?php echo TXT_FRAMASOFT_COUNT_SERVICES ;?></div>
											</div>
										</div>
									</h4>

								</div>
							</div>
							<!-- 3rd Stat Framasoft -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="row">
											<div class="col-md-3 col-xs-2">
												<div class="statToFill statInTitle framastats_members"><i class="fa fa-spinner" aria-hidden="true"></i></div>
											</div>
											<div class="col-md-8 col-xs-8">
												<div class="statNameInTitle"><?php echo TXT_FRAMASOFT_MEMBERS ;?></div>
											</div>
										</div>
									</h4>

								</div>
							</div>
							<!-- 4th Stat Framasoft -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="row">
											<div class="col-md-3 col-xs-2">
												<div class="statToFill statInTitle framastats_contributors"><i class="fa fa-spinner" aria-hidden="true"></i></div>
											</div>
											<div class="col-md-8 col-xs-8">
												<div class="statNameInTitle"><?php echo TXT_FRAMASOFT_CONTRIBUTORS ;?></div>
											</div>
										</div>
									</h4>

								</div>
							</div>
							<!-- 5th Stat Framasoft -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseTwo_framasoft" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framastats_donations"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMASOFT_DONATION ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseTwo_framasoft" > <!-- add 'in' if we want to be open at start-->
									<div class="panel-body">
										<p><?php echo TXT_FRAMASOFT_REGULAR_DONATION_1 ;?><strong><span class="statToFill framastats_recurrentDonations"> </span></strong><?php echo TXT_FRAMASOFT_REGULAR_DONATION_2 ;?></p>
									</div>
								</div>
							</div>
							<hr/>
							<!-- 6th Stat Framasoft -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseThree_framasoft" id="framasoft_chartAll_click" aria-haspopup="true" aria-expanded="false"> 
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row">
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framastats_rest_json_nbVisits_all_total"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMASOFT_VISIT_ALL_TITLE ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse" id="collapseThree_framasoft" > <!-- add 'in' if we want to be open at start-->
									<div class="panel-body">
										<h6><?php echo TXT_FRAMASOFT_VISIT_ALL_TEXT_1 ;?><a href="http://www.piwik.org/">Piwik</a>.</h6>
										<ul class="list-group"> 
										  <li class="list-group-item"> <span class="badge statToFill framastats_rest_json_nbVisits_thisYear_total">...</span> <?php echo TXT_THIS_YEAR ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framastats_rest_json_nbVisits_last365days_total">...</span> <?php echo TXT_1_LAST_YEAR ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framastats_rest_json_nbVisits_last180days_total">...</span> <?php echo TXT_6_LAST_MONTHS ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framastats_rest_json_nbVisits_last90days_total">...</span> <?php echo TXT_3_LAST_MONTHS ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framastats_rest_json_nbVisits_last30days_total">...</span> <?php echo TXT_1_LAST_MONTH ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framastats_rest_json_nbVisits_last7days_total">...</span> <?php echo TXT_1_LAST_WEEK ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framastats_rest_json_nbVisits_yesterday_total">...</span> <?php echo TXT_1_LAST_DAY ;?> </li>
										</ul>
									</div>
								</div>
								<div>
									<canvas class="notDisplayChart charts" id="framasoft_chartAll" width="300" height="150"></canvas> <!-- Change id : will be used in javascript. Use the same as you right in calculateChartsStats.php-->
								</div>
							</div>
							<!-- 7th Stat Framasoft -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseFour_soft" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row">
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framastats_rest_json_nbVisits_today_total_AVG"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMASOFT_STAT_AVG_STAT ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseFour_soft" >
									<div class="panel-body">
										<p><?php echo TXT_FRAMASOFT_STAT_AVG_TXT_1 ;?><strong><span class="statToFill framastats_TOP_rest_json_nbVisits_today_total_date"> </span></strong><?php echo TXT_FRAMASOFT_STAT_AVG_TXT_2 ;?><strong><span class="statToFill framastats_TOP_rest_json_nbVisits_today_total_value"> </span></strong><?php echo TXT_FRAMASOFT_STAT_AVG_TXT_3 ;?></p>
									</div>
								</div>
							</div>
							<!-- 8th Stat Framasoft -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#" id="framasoft_chartToday_click" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row">
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framastats_rest_json_nbVisits_today_total"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMASOFT_VISIT_TODAY_TITLE ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<div>
									<canvas class="notDisplayChart charts" id="framasoft_chartToday" width="300" height="150"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Framadate -->
				<div id="Framadate" class="divsCenter">
					<div class="row serviceNameRow">
						<div class="col-md-8 col-xs-12">
							<h2>
							<i class="fa fa-fw fa-lg fa-calendar" aria-hidden="true"></i>
							Framadate
							</h2>
						</div>
						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large">
							<h6 class="textUpdate_large"><?php echo TXT_UPDATE ;?><span class="statToFill framadate_lastUpdate"></span> </h6>
						</div>
						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small">
							<h6><?php echo TXT_UPDATE ;?><span class="statToFill framadate_lastUpdate"></span></h6>
						</div>
					</div>
					<h6><?php echo TXT_FRAMADATE_DESCRIPTION . TXT_FRAMADATE_DESCRIPTION_LIEN ;?> <a href="https://www.framadate.org" target="_blank"> Framadate </a> </h6>
					<div class="centerStats">
						<div class="panel-group">

							<!-- 1st Stat Framadate -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseTwo_date" id="framadate_chartAll_click" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row">
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framadate_mySQL_nbPulls_all_created"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMADATE_STAT_ALL_TITLE ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse" id="collapseTwo_date" > <!-- add 'in' if we want to be open at start-->
									<div class="panel-body">
										<ul class="list-group">
										  <li class="list-group-item"> <span class="badge statToFill framadate_mySQL_nbPulls_6lastMonths_created">...</span> <?php echo TXT_6_LAST_MONTHS ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framadate_mySQL_nbPulls_3lastMonths_created">...</span> <?php echo TXT_3_LAST_MONTHS ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framadate_mySQL_nbPulls_1lastMonth_created">...</span> <?php echo TXT_1_LAST_MONTH ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framadate_mySQL_nbPulls_1lastWeek_created">...</span> <?php echo TXT_1_LAST_WEEK ;?> </li>
										</ul>
									</div>
								</div>
								<div>
									<canvas class="notDisplayChart charts" id="framadate_chartAll" width="300" height="150"></canvas> <!-- Change id : will be used in javascript. Use the same as you right in calculateChartsStats.php-->
								</div>
							</div>
							<!-- 2nd Stat Framadate -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseOne_date" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framadate_mySQL_nbPulls_today_created_AVG"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMADATE_STAT_AVG_STAT ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseOne_date" >
									<div class="panel-body">
										<p><?php echo TXT_FRAMADATE_STAT_AVG_TEXT_1 ;?><strong><span class="statToFill framadate_TOP_mySQL_nbPulls_today_created_date"> </span></strong><?php echo TXT_FRAMADATE_STAT_AVG_TEXT_2 ;?><strong><span class="statToFill framadate_TOP_mySQL_nbPulls_today_created_value"> </span></strong><?php echo TXT_FRAMADATE_STAT_AVG_TEXT_3 ;?></p>
									</div>
								</div>
							</div>
							<!-- 3rd Stat Framadate -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#" id="framadate_chartToday_click" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framadate_mySQL_nbPulls_today_created"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMADATE_STAT_TODAY_STAT ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<div>
									<canvas class="notDisplayChart charts" id="framadate_chartToday" width="300" height="150"></canvas>
								</div>
							</div>
							<hr/>
							<!-- 4th Stat Framadate -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="row">
											<div class="col-md-3 col-xs-2">
												<div class="statToFill statInTitle framadate_mySQL_pulls_lifeExpectancyInDays"><i class="fa fa-spinner" aria-hidden="true"></i></div>
											</div>
											<div class="col-md-8 col-xs-8">
												<div class="statNameInTitle"><?php echo TXT_FRAMADATE_STAT_LIFE_EXPECTANCY ;?></div>
											</div>
										</div>
									</h4>

								</div>
							</div>
							<!-- 5th Stat Framadate -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="row">
											<div class="col-md-3 col-xs-2">
												<div class="statToFill statInTitle framadate_mySQL_pulls_avgNbUsers"><i class="fa fa-spinner" aria-hidden="true"></i></div>
											</div>
											<div class="col-md-8 col-xs-8">
												<div class="statNameInTitle"><?php echo TXT_FRAMADATE_STAT_AVG_USERS ;?></div>
											</div>
										</div>
									</h4>

								</div>
							</div>
							<!-- 6th Stat Framadate -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseThree_date" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framadate_mySQL_pulls_formats_0_D"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMADATE_STAT_FORMAT_TITLE ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseThree_date" >
									<div class="panel-body">
										<ul class="list-group">
										  <li class="list-group-item"> <span class="badge"> <span class="statToFill framadate_mySQL_pulls_formats_1_A">...</span> %</span> <?php echo TXT_FRAMADATE_STAT_FORMAT_1; ?> </li>
										</ul> 
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Framapad -->
				<div id="Framapad" class="divsCenter">
					<div class="row serviceNameRow">
						<div class="col-md-8 col-xs-12">
							<h2>
							<i class="fa fa-fw fa-lg fa-align-left" aria-hidden="true"></i>
							Framapad
							</h2>
						</div>
						<!-- Large devices -->
						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 
							<h6 class="textUpdate_large"><?php echo TXT_UPDATE ;?><span class="statToFill framapad_lastUpdate"></span> </h6> 
						</div>
						<!-- Small devices -->
						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 
							<h6><?php echo TXT_UPDATE ;?><span class="statToFill framapad_lastUpdate"></span></h6> 
						</div>
					</div>
					<h6><?php echo TXT_FRAMAPAD_DESCRIPTION . TXT_FRAMAPAD_DESCRIPTION_LIEN ;?> <a href="https://www.framapad.org" target="_blank"> Framapad </a> </h6>
					<div class="centerStats">
						<div class="panel-group">
							<!-- 1st Stat Framapad -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseOne_pad" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framapad_rest_json_totalPads_living"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMAPAD_STAT_ALL_TITLE ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseOne_pad" > <!-- add 'in' if we want to be open at start-->
									<div class="panel-body">
										<h6><?php echo TXT_FRAMAPAD_STAT_ALL_TEXT ;?></h6>
										<ul class="list-group">
										  <li class="list-group-item"> <span class="badge statToFill framapad_rest_json_pluginFramapad_quotidien_padsCount">...</span> <?php echo TXT_PADS_DAILY ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framapad_rest_json_pluginFramapad_hebdo_padsCount">...</span> <?php echo TXT_PADS_WEEKLY ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framapad_rest_json_pluginFramapad_mensuel_padsCount">...</span> <?php echo TXT_PADS_MONTHLY ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framapad_rest_json_pluginFramapad_bimestriel_padsCount">...</span> <?php echo TXT_PADS_BIMONTHLY ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framapad_rest_json_pluginFramapad_semestriel_padsCount">...</span> <?php echo TXT_PADS_BIANNUAL ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framapad_rest_json_pluginFramapad_annuel_padsCount">...</span> <?php echo TXT_PADS_ANNUAL ;?> </li>
										</ul>
									</div>
								</div>
							</div>
							<!-- 2nd Stat Framapad -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="row">
											<div class="col-md-3 col-xs-2">
												<div class="statToFill statInTitle framapad_rest_json_totalBlankPads_living_percent"><i class="fa fa-spinner" aria-hidden="true"></i></div>
											</div>
											<div class="col-md-8 col-xs-8">
												<div class="statNameInTitle"><?php echo TXT_FRAMAPAD_STAT_BLANK ;?></div>
											</div>
										</div>
									</h4>

								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Framapic -->
				<div id="Framapic" class="divsCenter">
					<div class="row serviceNameRow">
						<div class="col-md-8 col-xs-12">
							<h2>
							<i class="fa fa-fw fa-lg fa-photo" aria-hidden="true"></i>
							Framapic
							</h2>
						</div>
						<!-- Large devices -->
						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 
							<h6 class="textUpdate_large"><?php echo TXT_UPDATE ;?><span class="statToFill framapic_lastUpdate"></span></h6> 
						</div>
						<!-- Small devices -->
						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 
							<h6><?php echo TXT_UPDATE ;?><span class="statToFill framapic_lastUpdate"></span></h6> 
						</div>
					</div>
					<h6><?php echo TXT_FRAMAPIC_DESCRIPTION ;?><a href="https://framapic.org/" target="_blank"><?php echo TXT_FRAMAPIC_DESCRIPTION_LIEN; ?>.</a></h6>
					<div class="centerStats">
						<div class="panel-group">
						<!-- 1st Stat Framapic -->
						<div class="panel panel-default clickable">
							<!-- Panel heading clickable-->
							<a data-toggle="collapse" href="#" id="framapic_chartAll_click" aria-haspopup="true" aria-expanded="false">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="row">
											<div class="col-md-3 col-xs-2">
												<div class="statToFill statInTitle framapic_total"><i class="fa fa-spinner" aria-hidden="true"></i></div>
											</div>
											<div class="col-md-8 col-xs-8">
												<div class="statNameInTitle"><?php echo TXT_FRAMAPIC_TOTAL ;?></div>
											</div>
											<div class="col-md-1 col-xs-1 icon">
												<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
											</div>
										</div>
									</h4>
								</div>
							</a>
							<!-- Panel collapse-->
							<div>
								<canvas class="notDisplayChart charts" id="framapic_chartAll" width="300" height="150"></canvas>
							</div>
						</div>

						<!-- 2nd Stat Framapic -->
						<div class="panel panel-default clickable">
							<!-- Panel heading clickable-->
							<a data-toggle="collapse" href="#" id="framapic_chartAverage_click" aria-haspopup="true" aria-expanded="false">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="row">
											<div class="col-md-3 col-xs-2">
												<div class="statToFill statInTitle framapic_average"><i class="fa fa-spinner" aria-hidden="true"></i></div>
											</div>
											<div class="col-md-8 col-xs-8">
												<div class="statNameInTitle"><?php echo TXT_FRAMAPIC_AVERAGE ;?></div>
											</div>
											<div class="col-md-1 col-xs-1 icon">
												<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
											</div>
										</div>
									</h4>
								</div>
							</a>
							<div>
								<canvas class="notDisplayChart charts" id="framapic_chartAverage" width="300" height="150"></canvas>
							</div>
						</div>
					</div>
				</div>

				<!-- Peertube -->
				<div id="Peertube" class="divsCenter">
					<div class="row serviceNameRow">
						<div class="col-md-8 col-xs-12">
							<h2>
							<i class="fa fa-fw fa-lg fa-play" aria-hidden="true"></i>
							Peertube
							</h2>
						</div>
						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large">
							<h6 class="textUpdate_large"><?php echo TXT_UPDATE ;?><span class="statToFill peertube_lastUpdate"></span> </h6>
						</div>
						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small">
							<h6><?php echo TXT_UPDATE ;?><span class="statToFill peertube_lastUpdate"></span></h6>
						</div>
					</div>
					<h6><?php echo TXT_PEERTUBE_DESCRIPTION;?> <a href="https://joinpeertube.org/" target="_blank"> <?php echo TXT_PEERTUBE_DESCRIPTION_LIEN; ?>.</a> </h6>
					<div class="centerStats">
						<div class="panel-group">

							<!-- 1st Stat Peertube -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#" id="peertube_instances_click" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row">
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle peertube_totalInstances"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_PEERTUBE_INSTANCES ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<div>
									<canvas class="notDisplayChart charts" id="peertube_instances_users" width="300" height="150"></canvas>
								</div>
							</div>
							<!-- 2nd Stat Peertube -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#" id="peertube_videos_click" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle peertube_totalVideos"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_PEERTUBE_VIDEOS ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<div>
									<canvas class="notDisplayChart charts" id="peertube_videos" width="300" height="150"></canvas>
								</div>
							</div>
							<!-- 3rd Stat Peertube -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#" id="peertube_views_click" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle peertube_totalVideoViews"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_PEERTUBE_VIEWS ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<div>
									<canvas class="notDisplayChart charts" id="peertube_views" width="300" height="150"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- MyFrama -->
				<div id="MyFrama" class="divsCenter">
					<div class="row serviceNameRow">
						<div class="col-md-8 col-xs-12">
							<h2>
							<i class="fa fa-fw fa-lg fa-star"></i>
							MyFrama
							</h2>
						</div>
						<!-- Update time large devices -->
						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large">
							<h6 class="textUpdate_large"><?php echo TXT_UPDATE;?><span class="statToFill myframa_lastUpdate"></span> </h6>
						</div>
						<!-- Update time small devices -->
						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small">
							<h6><?php echo TXT_UPDATE;?><span class="statToFill myframa_lastUpdate"></span></h6>
						</div>
					</div>
					<h6><?php echo TXT_MYFRAMA_DESCRIPTION;?> <a href="https://my.framasoft.org/" target="_blank"> <?php echo TXT_MYFRAMA_DESCRIPTION_LIEN; ?>.</a> </h6>
					<div class="centerStats">
						<div class="panel-group">
							<!-- 1st Stat MyFrama -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#" id="myframa_chartAccount_click" aria-haspopup="true" aria-expanded="false">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="row">
											<div class="col-md-3 col-xs-2">
												<div class="statToFill statInTitle myframa_bash_commands_account_nb"><i class="fa fa-spinner" aria-hidden="true"></i></div>
											</div>
											<div class="col-md-8 col-xs-8">
												<div class="statNameInTitle"><?php echo TXT_MYFRAMA_ACCOUNT_NB ;?></div>
											</div>
											<div class="col-md-1 col-xs-1 icon">
												<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
											</div>
										</div>
									</h4>
								</div>
								</a>
								<div>
									<canvas class="notDisplayChart charts" id="myframa_chartAccount" width="300" height="150"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Framalibre -->
				<div id="Framalibre" class="divsCenter">
					<div class="row serviceNameRow">
						<div class="col-md-8 col-xs-12">
							<h2>
							<i class="fa fa-fw fa-lg fa-linux" aria-hidden="true"></i>
							Framalibre
							</h2>
						</div>
						<!-- Large devices -->
						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 
							<h6 class="textUpdate_large"><?php echo TXT_UPDATE ;?><span class="statToFill framalibre_lastUpdate"></span></h6> 
						</div>
						<!-- Small devices -->
						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 
							<h6><?php echo TXT_UPDATE ;?><span class="statToFill framalibre_lastUpdate"></span></h6> 
						</div>
					</div>
					<h6><?php echo TXT_FRAMALIBRE_DESCRIPTION .TXT_FRAMALIBRE_DESCRIPTION_LIEN ;?> <a href="http://www.framalibre.org" target="_blank"> Framalibre </a> </h6>
					<div class="centerStats">
						<div class="panel-group">
							<!-- 1st Stat Framalibre -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseOne_libre" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framalibre_all_types"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMALIBRE_STAT_ALL_TYPES ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseOne_libre" > <!-- add 'in' if we want to be open at start-->
									<div class="panel-body">
										<ul class="list-group">
										  <li class="list-group-item"> <span class="badge statToFill framalibre_types_software_count">...</span> <?php echo TXT_LIBRE_SOFT_COUNT ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framalibre_types_faq_count">...</span> <?php echo TXT_LIBRE_FAQ_COUNT ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framalibre_types_hardware_count">...</span> <?php echo TXT_LIBRE_HARDWARE_COUNT ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framalibre_types_media_count">...</span> <?php echo TXT_LIBRE_MEDIA_COUNT ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framalibre_types_chronique_count">...</span> <?php echo TXT_LIBRE_CHRONIQUE_COUNT ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framalibre_types_article_count">...</span> <?php echo TXT_LIBRE_ARTICLE_COUNT ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framalibre_types_livre_count">...</span> <?php echo TXT_LIBRE_LIVRE_COUNT ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framalibre_types_other_count">...</span> <?php echo TXT_LIBRE_OTHER_COUNT ;?> </li>
										</ul>
									</div>
								</div>
							</div>

							<!-- 2nd Stat Framalibre -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseTwo_libre" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framalibre_all_types_contributors"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMALIBRE_STAT_ALL_TYPES_CONTRIB ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseTwo_libre" > <!-- add 'in' if we want to be open at start-->
									<div class="panel-body">
										<ul class="list-group">
										  <li class="list-group-item"> <span class="badge statToFill framalibre_types_software_contributors">...</span> <?php echo TXT_LIBRE_SOFT_CONTRIB ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framalibre_types_faq_contributors">...</span> <?php echo TXT_LIBRE_FAQ_CONTRIB ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framalibre_types_hardware_contributors">...</span> <?php echo TXT_LIBRE_HARDWARE_CONTRIB ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framalibre_types_media_contributors">...</span> <?php echo TXT_LIBRE_MEDIA_CONTRIB ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framalibre_types_chronique_contributors">...</span> <?php echo TXT_LIBRE_CHRONIQUE_CONTRIB ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framalibre_types_article_contributors">...</span> <?php echo TXT_LIBRE_ARTICLE_CONTRIB ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framalibre_types_livre_contributors">...</span> <?php echo TXT_LIBRE_LIVRE_CONTRIB ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framalibre_types_other_contributors">...</span> <?php echo TXT_LIBRE_OTHER_CONTRIB ;?> </li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Framablog -->
				<div id="Framablog" class="divsCenter">
					<div class="row serviceNameRow">
						<div class="col-md-8 col-xs-12">
							<h2>
							<i class="fa fa-fw fa-lg fa-pencil" aria-hidden="true"></i>
							Framablog
							</h2>
						</div>
						<!-- Large devices -->
						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 
							<h6 class="textUpdate_large"><?php echo TXT_UPDATE ;?><span class="statToFill framablog_lastUpdate"></span></h6> 
						</div>
						<!-- Small devices -->
						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 
							<h6><?php echo TXT_UPDATE ;?><span class="statToFill framablog_lastUpdate"></span></h6> 
						</div>
					</div>
					<h6><?php echo TXT_FRAMABLOG_DESCRIPTION . TXT_FRAMABLOG_DESCRIPTION_LIEN ;?> <a href="http://www.framablog.org" target="_blank"> Framablog </a> </h6>
					<div class="centerStats">
						<div class="panel-group">
							<!-- 1st Stat Framablog -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseOne_blog" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framablog_mySQL_nbPosts_all"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMABLOG_STAT_POSTS_TITLE ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseOne_blog" >
									<div class="panel-body">
										<ul class="list-group">
										  <li class="list-group-item"> <span class="badge statToFill framablog_mySQL_nbPosts_1lastYear">...</span> <?php echo TXT_ADD_YEAR ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framablog_mySQL_nbPosts_6lastMonths">...</span> <?php echo TXT_ADD_6MONTHS ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framablog_mySQL_nbPosts_3lastMonths">...</span> <?php echo TXT_ADD_3MONTHS ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framablog_mySQL_nbPosts_1lastMonth">...</span> <?php echo TXT_ADD_1MONTH ;?> </li>
										</ul>
									</div>
								</div>
							</div>

							<!-- 2nd Stat Framablog -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseTwo_blog" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framablog_mySQL_nbCategory"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMABLOG_STAT_CATEGORY_TITLE ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseTwo_blog" >
									<div class="panel-body">
										<?php echo TXT_FRAMABLOG_STAT_CATEGORY_TEXT_1 ;?>
										<strong><span class="statToFill framablog_category_rank_0_name"></span></strong> (<span class="statToFill framablog_category_rank_0_value"></span>),
										<strong><span class="statToFill framablog_category_rank_1_name"></span></strong> (<span class="statToFill framablog_category_rank_1_value"></span>),
										<strong><span class="statToFill framablog_category_rank_2_name"></span></strong> (<span class="statToFill framablog_category_rank_2_value"></span>),
										<strong><span class="statToFill framablog_category_rank_3_name"></span></strong> (<span class="statToFill framablog_category_rank_3_value"></span>),
										<strong><span class="statToFill framablog_category_rank_4_name"></span></strong> (<span class="statToFill framablog_category_rank_4_value"></span>),
										<?php echo TXT_AND ;?><strong><span class="statToFill framablog_category_rank_5_name"></span></strong> (<span class="statToFill framablog_category_rank_5_value"></span>).
									</div>
								</div>
							</div>

							<!-- 3rd Stat Framablog -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseThree_blog" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framablog_mySQL_nbComments_all"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMABLOG_STAT_COMMENTS_TITLE ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseThree_blog" >
									<div class="panel-body">
										<ul class="list-group">
										  <li class="list-group-item"> <span class="badge statToFill framablog_mySQL_nbComments_1lastYear">...</span> <?php echo TXT_ADD_YEAR ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framablog_mySQL_nbComments_6lastMonths">...</span> <?php echo TXT_ADD_6MONTHS ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framablog_mySQL_nbComments_3lastMonths">...</span> <?php echo TXT_ADD_3MONTHS ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framablog_mySQL_nbComments_1lastMonth">...</span> <?php echo TXT_ADD_1MONTH ;?> </li>
										</ul>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>

				<!-- Framabook -->
				<div id="Framabook" class="divsCenter">
					<div class="row serviceNameRow">
						<div class="col-md-8 col-xs-12">
							<h2>
							<i class="fa fa-fw fa-lg fa-book" aria-hidden="true"></i>
							Framabook
							</h2>
						</div>
						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 
							<h6 class="textUpdate_large"><?php echo TXT_UPDATE ;?><span class="statToFill framabook_lastUpdate"></span> </h6> 
						</div>
						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 
							<h6><?php echo TXT_UPDATE ;?><span class="statToFill framabook_lastUpdate"></span></h6> 
						</div>
					</div>
					<h6><?php echo TXT_FRAMABOOK_DESCRIPTION . TXT_FRAMABOOK_DESCRIPTION_LIEN ;?> <a href="http://www.framabook.org" target="_blank"> Framabook </a> </h6>
					<div class="centerStats">
						<div class="panel-group">

							<!-- 1st Stat Framabook -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="row">
											<div class="col-md-3 col-xs-2">
												<div class="statToFill statInTitle framabook_mySQL_nbBooks"><i class="fa fa-spinner" aria-hidden="true"></i></div>
											</div>
											<div class="col-md-8 col-xs-8">
												<div class="statNameInTitle"><?php echo TXT_FRAMABOOK_STAT_BOOKS ;?></div>
											</div>
										</div>
									</h4>
								</div>
							</div>
							<hr/>
							<!-- 2nd Stat Framabook -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseOne_book">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framabook_mySQL_nbDwnlds_all"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMABOOK_STAT_ALL_TITLE ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseOne_book" > <!-- add 'in' if we want to be open at start-->
									<div class="panel-body">
										<ul class="list-group">
										  <li class="list-group-item"> <span class="badge statToFill framabook_mySQL_nbDwnlds_6lastMonths">...</span> <?php echo TXT_6_LAST_MONTHS ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framabook_mySQL_nbDwnlds_3lastMonths">...</span> <?php echo TXT_3_LAST_MONTHS ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framabook_mySQL_nbDwnlds_1lastMonth">...</span> <?php echo TXT_1_LAST_MONTH ;?> </li>
										  <li class="list-group-item"> <span class="badge statToFill framabook_mySQL_nbDwnlds_1lastWeek">...</span> <?php echo TXT_1_LAST_WEEK ;?> </li>
										</ul>
									</div>
								</div>
							</div>
							<hr/>
							<!-- 5th Stat Framabook -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseThree_book" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMABOOK_STAT_TOP_DOWNLDS ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseThree_book" >
									<div class="panel-body">
										<ul class="list-group">
										  <li class="list-group-item"> <span class="badge"> <span class="statToFill framabook_downloadsAll_rank_0_value">...</span> </span> <span class="statToFill framabook_downloadsAll_rank_0_name">...</span> </li>
										  <li class="list-group-item"> <span class="badge"> <span class="statToFill framabook_downloadsAll_rank_1_value">...</span> </span> <span class="statToFill framabook_downloadsAll_rank_1_name">...</span> </li>
										  <li class="list-group-item"> <span class="badge"> <span class="statToFill framabook_downloadsAll_rank_2_value">...</span> </span> <span class="statToFill framabook_downloadsAll_rank_2_name">...</span> </li>
										  <li class="list-group-item"> <span class="badge"> <span class="statToFill framabook_downloadsAll_rank_3_value">...</span> </span> <span class="statToFill framabook_downloadsAll_rank_3_name">...</span> </li>
										  <li class="list-group-item"> <span class="badge"> <span class="statToFill framabook_downloadsAll_rank_4_value">...</span> </span> <span class="statToFill framabook_downloadsAll_rank_4_name">...</span> </li>
										  <li class="list-group-item"> <span class="badge"> <span class="statToFill framabook_downloadsAll_rank_5_value">...</span> </span> <span class="statToFill framabook_downloadsAll_rank_5_name">...</span> </li>
										  <li class="list-group-item"> <span class="badge"> <span class="statToFill framabook_downloadsAll_rank_6_value">...</span> </span> <span class="statToFill framabook_downloadsAll_rank_6_name">...</span> </li>
										  <li class="list-group-item"> <span class="badge"> <span class="statToFill framabook_downloadsAll_rank_7_value">...</span> </span> <span class="statToFill framabook_downloadsAll_rank_7_name">...</span> </li>
										  <li class="list-group-item"> <span class="badge"> <span class="statToFill framabook_downloadsAll_rank_8_value">...</span> </span> <span class="statToFill framabook_downloadsAll_rank_8_name">...</span> </li>
										  <li class="list-group-item"> <span class="badge"> <span class="statToFill framabook_downloadsAll_rank_9_value">...</span> </span> <span class="statToFill framabook_downloadsAll_rank_9_name">...</span> </li>
										</ul> 
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Framasphere -->
				<div id="Framasphere" class="divsCenter">
					<div class="row serviceNameRow">
						<div class="col-md-8 col-xs-12">
							<h2>
							<i class="fa fa-fw fa-lg fa-asterisk" aria-hidden="true"></i>
							Framasphere
							</h2>
						</div>
						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 
							<h6 class="textUpdate_large"><?php echo TXT_UPDATE ;?><span class="statToFill framasphere_lastUpdate"></span> </h6> 
						</div>
						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 
							<h6><?php echo TXT_UPDATE ;?><span class="statToFill framasphere_lastUpdate"></span></h6> 
						</div>
					</div>
					<h6> <a href="https://www.framasphere.org" target="_blank">Framasphere</a> <?php echo TXT_FRAMASPHERE_DESCRIPTION;?>  </h6>
					<div class="centerStats">
						<div class="panel-group">

							<!-- 1st Stat Framasphere -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#" id="framasphere_users_click" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row">
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framasphere_rest_json_total_users"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMASPHERE_STAT_TOTAL_USERS ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<div>
									<canvas class="notDisplayChart charts" id="framasphere_users" width="300" height="150"></canvas>
								</div>
							</div>
							<!-- 2nd Stat Framasphere -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#" id="framasphere_posts_click" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framasphere_rest_json_local_posts"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMASPHERE_STAT_POSTS ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<div>
									<canvas class="notDisplayChart charts" id="framasphere_posts" width="300" height="150"></canvas>
								</div>
							</div>
							<!-- 3rd Stat Framasphere -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#" id="framasphere_comments_click" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framasphere_rest_json_local_comments"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMASPHERE_STAT_COMMENTS ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<div>
									<canvas class="notDisplayChart charts" id="framasphere_comments" width="300" height="150"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Framabin -->
				<div id="Framabin" class="divsCenter">
					<div class="row serviceNameRow">
						<div class="col-md-8 col-xs-12">
							<h2>
							<i class="fa fa-fw fa-lg fa-paste"></i>
							Framabin
							</h2>
						</div>
						<!-- Update time large devices -->
						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 
							<h6 class="textUpdate_large"><?php echo TXT_UPDATE;?><span class="statToFill framabin_lastUpdate"></span> </h6>
						</div>
						<!-- Update time small devices -->
						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 
							<h6><?php echo TXT_UPDATE;?><span class="statToFill framabin_lastUpdate"></span></h6>
						</div>
					</div>
					<h6><?php echo TXT_FRAMABIN_DESCRIPTION;?> <a href="https://framabin.org/" target="_blank"> <?php echo TXT_FRAMABIN_DESCRIPTION_LIEN; ?>.</a> </h6>
					<div class="centerStats">
						<div class="panel-group">
							<!-- 1st Stat Framabin -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<div class="row">
											<div class="col-md-3 col-xs-2">
												<div class="statToFill statInTitle framabin_logs_docCreated_all"><i class="fa fa-spinner" aria-hidden="true"></i></div>
											</div>
											<div class="col-md-8 col-xs-8">
												<div class="statNameInTitle"><?php echo TXT_FRAMABIN_DOCS_ALL_TITLE ;?></div>
											</div>
										</div>
									</h4>
								</div>
							</div>
							<!-- 2nd Stat Framabin -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseOne_framabin" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framabin_logs_docCreated_today"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle">
														<?php echo TXT_FRAMABIN_DOCS_TODAY ;?> 
													</div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseOne_framabin" >
									<div class="panel-body">
										<p><?php echo TXT_FRAMABIN_DOCS_TODAY_1; ?><strong><span class="statToFill framabin_TOP_logs_docCreated_today_date"></span></strong><?php echo TXT_FRAMABIN_DOCS_TODAY_2 ;?><strong><span class="statToFill framabin_TOP_logs_docCreated_today_value"></span></strong><?php echo TXT_FRAMABIN_DOCS_TODAY_3 ;?></p>
									</div>
								</div>
							</div>
							<hr/>
							<!-- 3rd Stat Framabin -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#" id="framabin_chartDocs_click" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row">
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framabin_logs_sharedDocs_all"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMABIN_SHARED_DOCS_ALL_TITLE ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<div>
									<canvas class="notDisplayChart charts" id="framabin_chartDocs" width="300" height="150"></canvas>
								</div>
							</div>
							<!-- 4st Stat Framabin -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseTwo_framabin" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row"> 
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framabin_logs_sharedDocs_today"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle">
														<?php echo TXT_FRAMABIN_SHARED_DOCS_TODAY ;?> 
													</div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron" id="collapseTwo_framabin" >
									<div class="panel-body">
										<p><?php echo TXT_FRAMABIN_SHARED_DOCS_TODAY_1; ?><strong><span class="statToFill framabin_TOP_logs_sharedDocs_today_date"></span></strong><?php echo TXT_FRAMABIN_SHARED_DOCS_TODAY_2 ;?><strong><span class="statToFill framabin_TOP_logs_sharedDocs_today_value"></span></strong><?php echo TXT_FRAMABIN_SHARED_DOCS_TODAY_3 ;?></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

                                <!-- Pattern Framaslides -->
                                <div id="Framaslides" class="divsCenter">
                                        <div class="row serviceNameRow">
                                                <div class="col-md-8 col-xs-12">
                                                        <h2>
							<i class="fa fa-fw fa-lg fa-pie-chart" aria-hidden="true"></i>
                                                        Framaslides
                                                        </h2>
                                                </div>
                                                <!-- Update time large devices -->
                                                <div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 
                                                        <h6 class="textUpdate_large"><?php echo TXT_UPDATE;?><span class="statToFill framaslides_lastUpdate"></span> </h6> <!-- Change name of service -->
                                                </div>
                                                <!-- Update time small devices -->
                                                <div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 
                                                        <h6><?php echo TXT_UPDATE;?><span class="statToFill framaslides_lastUpdate"></span></h6> <!-- Change name of service -->
                                                </div>
                                        </div>
                                        <h6><?php echo TXT_FRAMASLIDES_DESCRIPTION;?> <a href="http://framaslides.org/" target="_blank"> <?php echo TXT_FRAMASLIDES_DESCRIPTION_LIEN; ?>.</a> </h6> <!-- Change link href and constant -->
                                        <div class="centerStats">
                                                <div class="panel-group">
							<!-- 1st Stat Framaslides -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#" id="framaslides_graphe_click" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row">
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framaslides_rest_json_graphe"></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMASLIDES_GRAPHE ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<div>
									<canvas class="notDisplayChart charts" id="framaslides_graphe" width="300" height="150"></canvas>
								</div>
							</div>

                                                        <!-- Pattern : all kind of panels -->
                                                        <!-- 2nd Stat Framaslides --> <!-- Title with stat = Framasoft 2nd stat -->
                                                        <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                                <div class="row">
                                                                                        <div class="col-md-3 col-xs-2">
                                                                                                <div class="statToFill statInTitle framaslides_rest_json_presentations"><?php echo TXT_SHOULD_NOT_APPEAR; ?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->
                                                                                        </div>
                                                                                        <div class="col-md-8 col-xs-8">
                                                                                                <div class="statNameInTitle"><?php echo TXT_FRAMASLIDES_PRESENTATIONS ;?></div> <!-- Change constant -->
                                                                                        </div>
                                                                                </div>
                                                                        </h4>

                                                                </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                                <div class="row">
                                                                                        <div class="col-md-3 col-xs-2">
                                                                                                <div class="statToFill statInTitle framaslides_rest_json_enabled_users"><?php echo TXT_SHOULD_NOT_APPEAR; ?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->
                                                                                        </div>
                                                                                        <div class="col-md-8 col-xs-8">
                                                                                                <div class="statNameInTitle"><?php echo TXT_FRAMASLIDES_ENABLED_USERS ;?></div> <!-- Change constant -->
                                                                                        </div>
                                                                                </div>
                                                                        </h4>

                                                                </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                                <div class="row">
                                                                                        <div class="col-md-3 col-xs-2">
                                                                                                <div class="statToFill statInTitle framaslides_rest_json_groups"><?php echo TXT_SHOULD_NOT_APPEAR; ?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->
                                                                                        </div>
                                                                                        <div class="col-md-8 col-xs-8">
                                                                                                <div class="statNameInTitle"><?php echo TXT_FRAMASLIDES_GROUPS ;?></div> <!-- Change constant -->
                                                                                        </div>
                                                                                </div>
                                                                        </h4>

                                                                </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                                <div class="row">
                                                                                        <div class="col-md-3 col-xs-2">
                                                                                                <div class="statToFill statInTitle framaslides_rest_json_public_templates"><?php echo TXT_SHOULD_NOT_APPEAR; ?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->
                                                                                        </div>
                                                                                        <div class="col-md-8 col-xs-8">
                                                                                                <div class="statNameInTitle"><?php echo TXT_FRAMASLIDES_TEMPLATES ;?></div> <!-- Change constant -->
                                                                                        </div>
                                                                                </div>
                                                                        </h4>

                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                </div>

                                <!-- Pattern Framapiaf -->
                                <div id="Framapiaf" class="divsCenter">
                                        <div class="row serviceNameRow">
                                                <div class="col-md-8 col-xs-12">
                                                        <h2>
							<i class="fa fa-fw fa-lg fa-retweet" aria-hidden="true"></i>
                                                        Framapiaf
                                                        </h2>
                                                </div>
                                                <!-- Update time large devices -->
                                                <div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 
                                                        <h6 class="textUpdate_large"><?php echo TXT_UPDATE;?><span class="statToFill framapiaf_lastUpdate"></span> </h6> <!-- Change name of service -->
                                                </div>
                                                <!-- Update time small devices -->
                                                <div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 
                                                        <h6><?php echo TXT_UPDATE;?><span class="statToFill framapiaf_lastUpdate"></span></h6> <!-- Change name of service -->
                                                </div>
                                        </div>
                                        <h6><?php echo TXT_FRAMAPIAF_DESCRIPTION;?> <a href="http://framapiaf.org/" target="_blank"> <?php echo TXT_FRAMAPIAF_DESCRIPTION_LIEN; ?>.</a> </h6> <!-- Change link href and constant -->
                                        <div class="centerStats">
                                                <div class="panel-group">
							<!-- 1st Stat Framapiaf -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#" id="framapiaf_graphe_click" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row">
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle framapiaf_rest_json_graphe"></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_FRAMAPIAF_GRAPHE ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<div>
									<canvas class="notDisplayChart charts" id="framapiaf_graphe" width="300" height="150"></canvas>
								</div>
							</div>

                                                        <!-- Pattern : all kind of panels -->
                                                        <!-- 2nd Stat Framapiaf --> <!-- Title with stat = Framasoft 2nd stat -->
                                                        <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                                <div class="row">
                                                                                        <div class="col-md-3 col-xs-2">
                                                                                                <div class="statToFill statInTitle framapiaf_rest_json_users"><?php echo TXT_SHOULD_NOT_APPEAR; ?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->
                                                                                        </div>
                                                                                        <div class="col-md-8 col-xs-8">
                                                                                                <div class="statNameInTitle"><?php echo TXT_FRAMAPIAF_USERS ;?></div> <!-- Change constant -->
                                                                                        </div>
                                                                                </div>
                                                                        </h4>

                                                                </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                                <div class="row">
                                                                                        <div class="col-md-3 col-xs-2">
                                                                                                <div class="statToFill statInTitle framapiaf_rest_json_posts"><?php echo TXT_SHOULD_NOT_APPEAR; ?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->
                                                                                        </div>
                                                                                        <div class="col-md-8 col-xs-8">
                                                                                                <div class="statNameInTitle"><?php echo TXT_FRAMAPIAF_POSTS ;?></div> <!-- Change constant -->
                                                                                        </div>
                                                                                </div>
                                                                        </h4>

                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                </div>

				<!-- Ici peuvent être ajoutés automatiquement de nouvelles stats -->
				<!-- Pattern Framanews -->				<div id="Framanews" class="divsCenter">					<div class="row serviceNameRow">						<div class="col-md-8 col-xs-12">							<h2>							<i class="fa fa-fw fa-lg fa-newspaper-o" aria-hidden="true"></i> <!-- Change here icon with glyphicon or fontawesome icon -->							Framanews							</h2>						</div>						<!-- Update time large devices -->						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 							<h6 class="textUpdate_large"><?php echo TXT_UPDATE;?><span class="statToFill framanews_lastUpdate"></span> </h6> <!-- Change name of service -->						</div>						<!-- Update time small devices -->						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 							<h6><?php echo TXT_UPDATE;?><span class="statToFill framanews_lastUpdate"></span></h6> <!-- Change name of service -->						</div>					</div>					<h6>Lecteur de flux RSS en ligne <a href="http://framanews.org/" target="_blank"> <?php echo TXT_DESCRIPTION_LIEN; ?>.</a> </h6> <!-- Change link href and constant -->					<div class="centerStats">						<div class="panel-group"><div class="panel panel-default">	<div class="panel-heading">		<h4 class="panel-title">			<div class="row">			<div class="col-md-3 col-xs-2">				<div class="statToFill statInTitle framanews_flux_RSS"><?php echo TXT_SHOULD_NOT_APPEAR; ?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->				</div>				<div class="col-md-8 col-xs-8">					<div class="statNameInTitle">Flux RSS</div> <!-- Change constant -->					</div>				</div>		</h4>	</div></div> <div class="panel panel-default">	<div class="panel-heading">		<h4 class="panel-title">			<div class="row">			<div class="col-md-3 col-xs-2">				<div class="statToFill statInTitle framanews_utilisateurs"><?php echo TXT_SHOULD_NOT_APPEAR; ?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->				</div>				<div class="col-md-8 col-xs-8">					<div class="statNameInTitle">Utilisateurs</div> <!-- Change constant -->					</div>				</div>		</h4>	</div></div> <!-- TO REPLACE WITH STAT -> 							<!-- END OF AUTOMATICALLY ADDED STAT ->						</div>					</div>				</div> 
				<!-- Pattern Framanotes -->				<div id="Framanotes" class="divsCenter">					<div class="row serviceNameRow">						<div class="col-md-8 col-xs-12">							<h2>							<i class="fa fa-fw fa-lg fa-sticky-note" aria-hidden="true"></i> <!-- Change here icon with glyphicon or fontawesome icon -->							Framanotes							</h2>						</div>						<!-- Update time large devices -->						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 							<h6 class="textUpdate_large"><?php echo TXT_UPDATE;?><span class="statToFill framanotes_lastUpdate"></span> </h6> <!-- Change name of service -->						</div>						<!-- Update time small devices -->						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 							<h6><?php echo TXT_UPDATE;?><span class="statToFill framanotes_lastUpdate"></span></h6> <!-- Change name of service -->						</div>					</div>					<h6>Emmenez vos notes avec vous, partout ! <a href="http://framanotes.org/" target="_blank"> <?php echo TXT_DESCRIPTION_LIEN; ?>.</a> </h6> <!-- Change link href and constant -->					<div class="centerStats">						<div class="panel-group"><div class="panel panel-default">	<div class="panel-heading">		<h4 class="panel-title">			<div class="row">			<div class="col-md-3 col-xs-2">				<div class="statToFill statInTitle framanotes_notes"><?php echo TXT_SHOULD_NOT_APPEAR; ?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->				</div>				<div class="col-md-8 col-xs-8">					<div class="statNameInTitle">Notes</div> <!-- Change constant -->					</div>				</div>		</h4>	</div></div> <div class="panel panel-default">	<div class="panel-heading">		<h4 class="panel-title">			<div class="row">			<div class="col-md-3 col-xs-2">				<div class="statToFill statInTitle framanotes_utilisateurs"><?php echo TXT_SHOULD_NOT_APPEAR; ?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->				</div>				<div class="col-md-8 col-xs-8">					<div class="statNameInTitle">Utilisateurs</div> <!-- Change constant -->					</div>				</div>		</h4>	</div></div> <!-- TO REPLACE WITH STAT -> 							<!-- END OF AUTOMATICALLY ADDED STAT ->						</div>					</div>				</div> 
				<!-- Pattern Framemo -->				<div id="Framemo" class="divsCenter">					<div class="row serviceNameRow">						<div class="col-md-8 col-xs-12">							<h2>							<i class="fa fa-fw fa-lg fa-object-group" aria-hidden="true"></i> <!-- Change here icon with glyphicon or fontawesome icon -->							Framemo							</h2>						</div>						<!-- Update time large devices -->						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 							<h6 class="textUpdate_large"><?php echo TXT_UPDATE;?><span class="statToFill framemo_lastUpdate"></span> </h6> <!-- Change name of service -->						</div>						<!-- Update time small devices -->						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 							<h6><?php echo TXT_UPDATE;?><span class="statToFill framemo_lastUpdate"></span></h6> <!-- Change name of service -->						</div>					</div>					<h6>Mettez vos idées au clair. <a href="http://framemo.org/" target="_blank"> <?php echo TXT_DESCRIPTION_LIEN; ?>.</a> </h6> <!-- Change link href and constant -->					<div class="centerStats">						<div class="panel-group"><div class="panel panel-default">	<div class="panel-heading">		<h4 class="panel-title">			<div class="row">			<div class="col-md-3 col-xs-2">				<div class="statToFill statInTitle framemo_nombre_de_tableaux"><?php echo TXT_SHOULD_NOT_APPEAR; ?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->				</div>				<div class="col-md-8 col-xs-8">					<div class="statNameInTitle">Nombre de tableaux</div> <!-- Change constant -->					</div>				</div>		</h4>	</div></div> <!-- TO REPLACE WITH STAT -> 							<!-- END OF AUTOMATICALLY ADDED STAT ->						</div>					</div>				</div> 
				<!-- Pattern Framacarte -->				<div id="Framacarte" class="divsCenter">					<div class="row serviceNameRow">						<div class="col-md-8 col-xs-12">							<h2>							<i class="fa fa-fw fa-lg fa-map" aria-hidden="true"></i> <!-- Change here icon with glyphicon or fontawesome icon -->							Framacarte							</h2>						</div>						<!-- Update time large devices -->						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 							<h6 class="textUpdate_large"><?php echo TXT_UPDATE;?><span class="statToFill framacarte_lastUpdate"></span> </h6> <!-- Change name of service -->						</div>						<!-- Update time small devices -->						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 							<h6><?php echo TXT_UPDATE;?><span class="statToFill framacarte_lastUpdate"></span></h6> <!-- Change name of service -->						</div>					</div>					<h6>Créez vos cartes personnalisées en ligne. <a href="http://framacarte.org/" target="_blank"> <?php echo TXT_DESCRIPTION_LIEN; ?>.</a> </h6> <!-- Change link href and constant -->					<div class="centerStats">						<div class="panel-group"><div class="panel panel-default">	<div class="panel-heading">		<h4 class="panel-title">			<div class="row">			<div class="col-md-3 col-xs-2">				<div class="statToFill statInTitle framacarte_cartes"><?php echo TXT_SHOULD_NOT_APPEAR; ?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->				</div>				<div class="col-md-8 col-xs-8">					<div class="statNameInTitle">Cartes</div> <!-- Change constant -->					</div>				</div>		</h4>	</div></div> <div class="panel panel-default">	<div class="panel-heading">		<h4 class="panel-title">			<div class="row">			<div class="col-md-3 col-xs-2">				<div class="statToFill statInTitle framacarte_comptes"><?php echo TXT_SHOULD_NOT_APPEAR; ?></div> <!-- Change class with stat name but don't touch at "statToFill statInTitle"-->				</div>				<div class="col-md-8 col-xs-8">					<div class="statNameInTitle">Comptes</div> <!-- Change constant -->					</div>				</div>		</h4>	</div></div> <!-- TO REPLACE WITH STAT -> 							<!-- END OF AUTOMATICALLY ADDED STAT ->						</div>					</div>				</div> 
<!-- NEXT STATS --> 
				<!-- Réseaux Sociaux -->
				<div id="ReseauxSociaux" class="divsCenter">
					<div class="row serviceNameRow">
						<div class="col-md-8 col-xs-12">
							<h2>
							<span class="glyphicon glyphicon-globe" aria-hidden="true"></span>
							Réseaux Sociaux
							</h2>
						</div>
						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 
							<h6 class="textUpdate_large"><?php echo TXT_UPDATE ;?><span class="statToFill reseauxSociaux_lastUpdate"></span> </h6> 
						</div>
						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 
							<h6><?php echo TXT_UPDATE ;?><span class="statToFill reseauxSociaux_lastUpdate"></span></h6> 
						</div>
					</div>
					<h6>
						<?php echo TXT_SOCIAL_NETWORKS_DESCRIPTION ;?> <a href="https://framasphere.org/u/framasoft" target="_blank">Diaspora</a>
						<?php echo TXT_SOCIAL_NETWORKS_DESCRIPTION_LIEN_1 ;?> <a href="https://twitter.com/framasoft" target="_blank">Twitter</a>,<a href="https://www.facebook.com/framasoft" target="_blank"> Facebook</a>. 
					</h6>
					<div class="centerStats">
						<div class="panel-group">

							<!-- 1st Stat Réseaux Sociaux -->
							<div class="panel panel-default clickable">
								<a data-toggle="collapse" href="#" id="twitter_followers_click" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row">
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle reseauxSociaux_twitter_nbFollowers"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_SOCIAL_NETWORKS_STAT_TWITTER_1 ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>

									</div>
								</a>
								<div>
									<canvas class="notDisplayChart charts" id="twitter_followers" width="300" height="150"></canvas>
								</div>
							</div>

							<!-- 2nd Stat Réseaux Sociaux -->
							<div class="panel panel-default clickable">
								<a data-toggle="collapse" href="#" id="twitter_tweets_click" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row">
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle reseauxSociaux_twitter_nbTweets"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_SOCIAL_NETWORKS_STAT_TWITTER_2 ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>

									</div>
								</a>
								<div>
									<canvas class="notDisplayChart charts" id="twitter_tweets" width="300" height="150"></canvas>
								</div>
							</div>

							<!-- 3rd Stat Réseaux Sociaux
							<div class="panel panel-default clickable">
								<a data-toggle="collapse" href="#" id="facebook_likes_click" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row">
												<div class="col-md-3 col-xs-2">
													<div class="statToFill statInTitle reseauxSociaux_facebook_nbLikes"><i class="fa fa-spinner" aria-hidden="true"></i></div>
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_SOCIAL_NETWORKS_STAT_FACEBOOK_1 ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-stats" aria-hidden="true" <?php echo TXT_LABEL_ICON_STAT ;?>></span>
												</div>
											</div>
										</h4>

									</div>
								</a>
								<div>
									<canvas class="notDisplayChart charts" id="facebook_likes" width="300" height="150"></canvas>
								</div>
							</div>-->
						</div>
					</div>
				</div>

				<!-- Other stats -->
				<div id="AutresStats" class="divsCenter">
					<div class="row serviceNameRow">
						<div class="col-md-8 col-xs-12">
							<h2>
							<span class="glyphicon glyphicon-stats" aria-hidden="true"></span>
							Autres statistiques
							</h2>
						</div>
						<!-- Update time large devices -->
						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large">
							<h6 class="textUpdate_large"><?php echo TXT_UPDATE;?><span class="statToFill framastats_lastUpdate"></span> </h6>
						</div>
						<!-- Update time small devices -->
						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small">
							<h6><?php echo TXT_UPDATE;?><span class="statToFill framastats_lastUpdate"></span></h6>
						</div>
					</div>
					<div class="centerStats">
						<div class="panel-group">
							<!-- 1st Stat FramaOther -->
							<div class="panel panel-default clickable">
								<!-- Panel heading clickable-->
								<a data-toggle="collapse" href="#collapseOne_other" aria-haspopup="true" aria-expanded="false">
									<div class="panel-heading">
										<h4 class="panel-title">
											<div class="row">
												<div class="col-md-3 col-xs-2">
												</div>
												<div class="col-md-8 col-xs-8">
													<div class="statNameInTitle"><?php echo TXT_OTHER_STATS_RANK_TITLE ;?></div>
												</div>
												<div class="col-md-1 col-xs-1 icon">
													<span class="glyphicon glyphicon-chevron-down" aria-hidden="true" <?php echo TXT_LABEL_CHEVRON_DOWN ;?>></span>
												</div>
											</div>
										</h4>
									</div>
								</a>
								<!-- Panel collapse-->
								<div class="panel-collapse collapse withChevron in" id="collapseOne_other" >
									<div class="panel-body">
										<ul class="list-group">
										  <li class="list-group-item"> <span class="badge statToFill rank_pwk_nbVisits_all_1_value">...</span> <span class="statToFill rank_pwk_nbVisits_all_1_service"> </li> 
										  <li class="list-group-item"> <span class="badge statToFill rank_pwk_nbVisits_all_2_value">...</span> <span class="statToFill rank_pwk_nbVisits_all_2_service">...</span> </li> 
										  <li class="list-group-item"> <span class="badge statToFill rank_pwk_nbVisits_all_3_value">...</span> <span class="statToFill rank_pwk_nbVisits_all_3_service">...</span> </li>
										  <li class="list-group-item"> <span class="badge statToFill rank_pwk_nbVisits_all_4_value">...</span> <span class="statToFill rank_pwk_nbVisits_all_4_service">...</span> </li>
										  <li class="list-group-item"> <span class="badge statToFill rank_pwk_nbVisits_all_5_value">...</span> <span class="statToFill rank_pwk_nbVisits_all_5_service">...</span> </li>
										  <li class="list-group-item"> <span class="badge statToFill rank_pwk_nbVisits_all_6_value">...</span> <span class="statToFill rank_pwk_nbVisits_all_6_service">...</span> </li>
										  <li class="list-group-item"> <span class="badge statToFill rank_pwk_nbVisits_all_7_value">...</span> <span class="statToFill rank_pwk_nbVisits_all_7_service">...</span> </li>
										  <li class="list-group-item"> <span class="badge statToFill rank_pwk_nbVisits_all_8_value">...</span> <span class="statToFill rank_pwk_nbVisits_all_8_service">...</span> </li>
										  <li class="list-group-item"> <span class="badge statToFill rank_pwk_nbVisits_all_9_value">...</span> <span class="statToFill rank_pwk_nbVisits_all_9_service">...</span> </li>
										  <li class="list-group-item"> <span class="badge statToFill rank_pwk_nbVisits_all_10_value">...</span> <span class="statToFill rank_pwk_nbVisits_all_10_service">...</span> </li>
										</ul>
										<strong><span class="statToFill rank_pwk_nbVisits_all_11_service"></span></strong> (<span class="statToFill rank_pwk_nbVisits_all_11_value"></span>), 
										<strong><span class="statToFill rank_pwk_nbVisits_all_12_service"></span></strong> (<span class="statToFill rank_pwk_nbVisits_all_12_value"></span>), 
										<strong><span class="statToFill rank_pwk_nbVisits_all_13_service"></span></strong> (<span class="statToFill rank_pwk_nbVisits_all_13_value"></span>), 
										<strong><span class="statToFill rank_pwk_nbVisits_all_14_service"></span></strong> (<span class="statToFill rank_pwk_nbVisits_all_14_value"></span>), 
										<strong><span class="statToFill rank_pwk_nbVisits_all_15_service"></span></strong> (<span class="statToFill rank_pwk_nbVisits_all_15_value"></span>), 
										<strong><span class="statToFill rank_pwk_nbVisits_all_16_service"></span></strong> (<span class="statToFill rank_pwk_nbVisits_all_16_value"></span>), 
										<strong><span class="statToFill rank_pwk_nbVisits_all_17_service"></span></strong> (<span class="statToFill rank_pwk_nbVisits_all_17_value"></span>), 
										<strong><span class="statToFill rank_pwk_nbVisits_all_18_service"></span></strong> (<span class="statToFill rank_pwk_nbVisits_all_18_value"></span>), 
										<strong><span class="statToFill rank_pwk_nbVisits_all_19_service"></span></strong> (<span class="statToFill rank_pwk_nbVisits_all_19_value"></span>)...
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- /All stats -->

				<!-- Framastats-nav for small devices-->
				<div role="navigation" class="visible-sm visible-xs">
					<hr/>
					<h2><?php echo TXT_NAVIGATION ;?></h2>
					<ul class="nav nav-pills nav-stacked">
						<li><a href="#Framasoft"><?php echo TXT_FRAMASOFT ;?></a></li>
						<li><a href="#Framadate">Framadate</a></li>
						<li><a href="#Framapad">Framapad</a></li>
						<li><a href="#Peertube">Peertube</a></li>
						<li><a href="#Framapic">Framapic</a></li>
						<li><a href="#Framalibre">Framalibre</a></li>
						<li><a href="#MyFrama">MyFrama</a></li>
						<li><a href="#Framablog">Framablog</a></li>
						<li><a href="#Framabook">Framabook</a></li>
						<li><a href="#Framasphere">Framasphere</a></li>
						<li><a href="#Framabin">Framabin</a></li>
						<li><a href="#Framaslides">Framaslides</a></li>
						<li><a href="#Framapiaf">Framapiaf</a></li>
<li><a href='#Framanews'>Framanews</a></li> 
<li><a href='#Framanotes'>Framanotes</a></li> 
<li><a href='#Framemo'>Framemo</a></li> 
<li><a href='#Framacarte'>Framacarte</a></li> 
<!-- NEXT LINK --> 
						<li><a href="#ReseauxSociaux"><?php echo TXT_SOCIAL_NETWORKS ;?></a></li>
						<li><a href="#AutresStats"><?php echo TXT_OTHER_STATS ;?></a></li>
					</ul>
				</div>
				<!-- /Framastats-nav for small devices -->
			</div>
		</div>
	</div>
	</main>

</body>
</html>
