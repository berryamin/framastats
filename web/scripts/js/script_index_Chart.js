//When DOM loaded
$(document).ready(function() {
	
	/*
	 * Handle the display of alls charts
	 * */
	function handleChart (chartId) {
		var selector = "#" + chartId;
		if ($(selector).hasClass("notDisplayChart")) {
			$(selector).removeClass("notDisplayChart");
			displayChart (chartId);
		} else {
			$(selector).addClass("notDisplayChart");
		}
	} 
	
	/*
	 * Called by handleChart
	 * Create chart according to their options
	 * */
	function displayChart (chartId) {

		// Retrieve charts stats
		var selectStats = ".chartsStats" + "." + chartId;
		var datasHTML 	= $(selectStats).html();
		var datasJSON 	= JSON.parse(datasHTML);

		var ctx = document.getElementById(chartId).getContext("2d");
		switch (chartId)
		{
			case 'framasoft_chartToday':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framasoft_chartAll':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'myframa_chartAccount':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framadate_chartToday':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framadate_chartAll':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'peertube_instances_users':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true,
					multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"
					});
				break;
			case 'peertube_videos':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true,
					multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"
					});
				break;
			case 'peertube_views':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true,
					multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"
					});
				break;
			case 'framapic_chartAverage':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framapic_chartAll':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framabook_chartToday':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framasphere_users':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true,
					multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"
					});
				break;
			case 'framasphere_posts':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framasphere_comments':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framadvd_chartAll':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'framabin_chartDocs':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true,
					multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"
					});
				break;
			case 'framaslides_graphe':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true,
					multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"
					});
				break;
			case 'framapiaf_graphe':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true,
					multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"
					});
				break;
			case 'twitter_followers':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'twitter_tweets':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
			case 'facebook_likes':
				var myNewChart = new Chart(ctx).Line(datasJSON, {
					responsive : true
					});
				break;
		}
	}

	$( "#framasoft_chartToday_click" ).click(function() {
		handleChart ('framasoft_chartToday');
	});
	$( "#framasoft_chartAll_click" ).click(function() {
		handleChart ('framasoft_chartAll');
	});
	$( "#myframa_chartAccount_click" ).click(function() {
		handleChart ('myframa_chartAccount');
	});
	$( "#framadate_chartToday_click" ).click(function() {
		handleChart ('framadate_chartToday');
	});
	$( "#framadate_chartAll_click" ).click(function() {
		handleChart ('framadate_chartAll');
	});
	$( "#peertube_instances_click" ).click(function() {
		handleChart ('peertube_instances_users');
	});
	$( "#peertube_videos_click" ).click(function() {
		handleChart ('peertube_videos');
	});
	$( "#peertube_views_click" ).click(function() {
		handleChart ('peertube_views');
	});
	$( "#framapic_chartAverage_click" ).click(function() {
		handleChart ('framapic_chartAverage');
	});
	$( "#framapic_chartAll_click" ).click(function() {
		handleChart ('framapic_chartAll');
	});
	$( "#framabook_chartToday_click" ).click(function() {
		handleChart ('framabook_chartToday');
	});
	$( "#framasphere_users_click" ).click(function() {
		handleChart ('framasphere_users');
	});
	$( "#framasphere_posts_click" ).click(function() {
		handleChart ('framasphere_posts');
	});
	$( "#framasphere_comments_click" ).click(function() {
		handleChart ('framasphere_comments');
	});
	$( "#framadvd_chartAll_click" ).click(function() {
		handleChart ('framadvd_chartAll');
	});
	$( "#framabin_chartDocs_click" ).click(function() {
		handleChart ('framabin_chartDocs');
	});
	$( "#framaslides_graphe_click" ).click(function() {
		handleChart ('framaslides_graphe');
	});
	$( "#framapiaf_graphe_click" ).click(function() {
		handleChart ('framapiaf_graphe');
	});
	$( "#twitter_followers_click" ).click(function() {
		handleChart ('twitter_followers');
	});
	$( "#twitter_tweets_click" ).click(function() {
		handleChart ('twitter_tweets');
	});
	$( "#facebook_likes_click" ).click(function() {
		handleChart ('facebook_likes');
	});

});


