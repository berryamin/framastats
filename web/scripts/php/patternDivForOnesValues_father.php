				<!-- Pattern Framaxxx -->
				<div id="Framaxxx" class="divsCenter">
					<div class="row serviceNameRow">
						<div class="col-md-8 col-xs-12">
							<h2>
							<span class="glyphicon glyphicon-tree-deciduous" aria-hidden="true"></span> <!-- Change here icon with glyphicon or fontawesome icon -->
							Framaxxx
							</h2>
						</div>
						<!-- Update time large devices -->
						<div class="col-md-4 visible-md-inline visible-lg-inline timeUpdate_large"> 
							<h6 class="textUpdate_large"><?php echo TXT_UPDATE;?><span class="statToFill framaxxx_lastUpdate"></span> </h6> <!-- Change name of service -->
						</div>
						<!-- Update time small devices -->
						<div class="visible-xs-block visible-sm-block col-xs-12 timeUpdate_small"> 
							<h6><?php echo TXT_UPDATE;?><span class="statToFill framaxxx_lastUpdate"></span></h6> <!-- Change name of service -->
						</div>
					</div>
					<h6> <!-- REPLACE_WITH_DESCRIPTION -->  <a href="http://framaxxx.org/" target="_blank"> <?php echo TXT_DESCRIPTION_LIEN; ?>.</a> </h6> <!-- Change link href and constant -->
					<div class="centerStats">
						<div class="panel-group">
							<!-- TO REPLACE WITH STAT ->
							<!-- END OF AUTOMATICALLY ADDED STAT ->
						</div>
					</div>
				</div>
