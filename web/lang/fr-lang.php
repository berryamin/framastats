<?php
/*
-----------------
Language: French
-----------------
*/

//---------------------------------------------------------
// index.php and other places
//---------------------------------------------------------

define('TXT_DESCRIPTION_SITE', "Outil public de statistiques pour l'ensemble des services mis en place par <a href='http://www.framasoft.org'>Framasoft</a>.");
define('TXT_FRAMASOFT', "Framasoft");
define('TXT_FRAMASOFT_NETWORK', "Framasoft");
define('TXT_SHOULD_NOT_APPEAR', "TXT_SHOULD_NOT_APPEAR");
define('TXT_LABEL_CHEVRON_DOWN', "aria-label='Afficher plus de statistiques'");
define('TXT_LABEL_ICON_STAT', "aria-label='Afficher plus de statistiques et des graphiques'");
define('TXT_NAVIGATION', "Navigation");
define('TXT_UPDATE', "Mise à jour : ");
define('TXT_AND', " et ");

define('TXT_THIS_YEAR', "Cette année :");
define('TXT_1_LAST_YEAR', "12 derniers mois :");
define('TXT_6_LAST_MONTHS', "6 derniers mois :");
define('TXT_3_LAST_MONTHS', "3 derniers mois :");
define('TXT_1_LAST_MONTH', "30 derniers jours :");
define('TXT_1_LAST_WEEK', "7 derniers jours :");
define('TXT_1_LAST_DAY', "Hier :");
define('TXT_1_TODAY', "Aujourd'hui :");
define('TXT_ADD_YEAR', "Ajouts en une année");
define('TXT_ADD_6MONTHS', "Ajouts ces 6 derniers mois");
define('TXT_ADD_3MONTHS', "Ajouts ces 3 derniers mois");
define('TXT_ADD_1MONTH', "Ajouts en 1 mois");

define('TXT_DESCRIPTION_LIEN', "Service accessible ici");

//---------------------------------------------------------
// Framasoft
//---------------------------------------------------------

define('TXT_FRAMASOFT_DESCRIPTION', "Framasoft est un réseau dédié à la promotion du « libre » en général et du logiciel libre en particulier. Depuis 2014, l'association s'est lancée dans le défi fou de ");
define('TXT_FRAMASOFT_DESCRIPTION_LIEN', "dégoogliser Internet");
define('TXT_FRAMASOFT_YEARS', "Années d'existence du réseau Framasoft");
define('TXT_FRAMASOFT_YEARS_ORG_1', "Et "); // Et .. 11 .. années d'existence
define('TXT_FRAMASOFT_YEARS_ORG_2', " années d'existence de l'association Framasoft (2004).");
define('TXT_FRAMASOFT_COUNT_SERVICES', "Projets actifs");
define('TXT_FRAMASOFT_IN_PROGRESS',  "Projets en cours");
define('TXT_FRAMASOFT_CONTRIBUTORS',  "Contributeur·trices par an");
define('TXT_FRAMASOFT_VISIT_ALL_TITLE', "Visites depuis la création de l'univers");
define('TXT_FRAMASOFT_VISIT_ALL_TEXT_1',  " \"création de l'univers\" = depuis 2013 et les premières mises en place de ");
define('TXT_FRAMASOFT_STAT_AVG_STAT', "Visites en moyenne par jour");
define('TXT_FRAMASOFT_STAT_AVG_TXT_1', "Jour le plus prolifique : "); 	// 30/08/1993
define('TXT_FRAMASOFT_STAT_AVG_TXT_2', "  avec ");				// 45000
define('TXT_FRAMASOFT_STAT_AVG_TXT_3', "  visites.");
define('TXT_FRAMASOFT_VISIT_TODAY_TITLE', "Visites aujourd'hui");
define('TXT_FRAMASOFT_MEMBERS',  "Membres");
define('TXT_FRAMASOFT_VOLUNTARY_WORK',  "Heures de bénévolat valorisé");
define('TXT_FRAMASOFT_DONATION',  "Euros de dons");
define('TXT_FRAMASOFT_REGULAR_DONATION_1',  "Dont "); // Dont 1200 € donateur·rice·s
define('TXT_FRAMASOFT_REGULAR_DONATION_2',  " donateur·rice·s par an.");


//---------------------------------------------------------
// MyFrama
//---------------------------------------------------------

define('TXT_MYFRAMA_DESCRIPTION', "Marquez et partagez vos liens.");
define('TXT_MYFRAMA_DESCRIPTION_LIEN', "Et hop, ça se passe ici.");
define('TXT_MYFRAMA_ACCOUNT_NB', "Comptes crées.");

//---------------------------------------------------------
// Framadate
//---------------------------------------------------------

define('TXT_FRAMADATE_DESCRIPTION', "Framadate c’est un peu comme Doodle mais en libre. Rien de plus simple de créer un rendez-vous ou un sondage sans inscription pour y inviter vos collaborateurs.");
define('TXT_FRAMADATE_DESCRIPTION_LIEN', " Ca se passe ici : ");
define('TXT_FRAMADATE_STAT_ALL_TITLE', "Sondages créés depuis la nuit des temps");
define('TXT_FRAMADATE_STAT_AVG_STAT', "Sondages crées en moyenne chaque jour");
define('TXT_FRAMADATE_STAT_AVG_TEXT_1', "Jour le plus prolifique : "); 	// 30/08/1993
define('TXT_FRAMADATE_STAT_AVG_TEXT_2', "  avec ");				// 69
define('TXT_FRAMADATE_STAT_AVG_TEXT_3', "  sondages créés.");
define('TXT_FRAMADATE_STAT_TODAY_STAT', "Sondages créés aujourd'hui");
define('TXT_FRAMADATE_STAT_LIFE_EXPECTANCY', "Durée de vie d'un sondage moyen (en jours)");
define('TXT_FRAMADATE_STAT_AVG_USERS', "Utilisateurs moyens par sondages");
define('TXT_FRAMADATE_STAT_FORMAT_TITLE', "% de sondages de type 'Date'");
define('TXT_FRAMADATE_STAT_FORMAT_1', "Sondages 'Classique'");
define('TXT_FRAMADATE_STAT_FORMAT_2', "Sondages 'Date' sans modifications possibles des utilisateurs");
define('TXT_FRAMADATE_STAT_FORMAT_3', "Sondages 'Classique' sans modifications possibles des utilisateurs");
define('TXT_FRAMADATE_STAT_FORMAT_TXT', "Le reste étant de vieux types de sondages tout démodés.");

//---------------------------------------------------------
// Framapad
//---------------------------------------------------------

define('TXT_FRAMAPAD_DESCRIPTION', "Écrivons tous ensemble sur le même document dans une interface de rédaction collective en ligne. Aucune inscription requise, on se donne un pseudo, une couleur et on se lance !");
define('TXT_FRAMAPAD_DESCRIPTION_LIEN', " Ca se passe ici : ");
define('TXT_FRAMAPAD_STAT_ALL_TITLE', "Pads actuellement actifs");
define('TXT_FRAMAPAD_STAT_ALL_TEXT', "Statistiques concernant les pads créés depuis la mise en place en 24 mars 2015 des pads temporaires et illimités.");
define('TXT_PADS_DAILY', "Pads quotidiens :");
define('TXT_PADS_WEEKLY', "Pads hebdomadaires :");
define('TXT_PADS_MONTHLY', "Pads mensuels :");
define('TXT_PADS_BIMONTHLY', "Pads bimensuels :");
define('TXT_PADS_BIANNUAL', "Pads semestriels :");
define('TXT_PADS_ANNUAL', "Pads annuels :");
define('TXT_PADS_ETERNAL', "Pads éternels :");
define('TXT_FRAMAPAD_STAT_BLANK', "% de pads vides");

//---------------------------------------------------------
// Peertube
//---------------------------------------------------------

define('TXT_PEERTUBE_DESCRIPTION', "L’hébergement de vidéos décentralisé, en réseau, basé sur du logiciel libre. Découvrez");
define('TXT_PEERTUBE_DESCRIPTION_LIEN', "Peertube ici");
define('TXT_PEERTUBE_INSTANCES', "Instances.");
define('TXT_PEERTUBE_VIDEOS', "Vidéos sur toutes les instances publiques.");
define('TXT_PEERTUBE_VIEWS', "Vues sur toutes les instances publiques.");

//---------------------------------------------------------
// Framapic
//---------------------------------------------------------

define('TXT_FRAMAPIC_DESCRIPTION', "Partagez des images de façon confidentielle.");
define('TXT_FRAMAPIC_DESCRIPTION_LIEN', " Cliquez ici");
define('TXT_FRAMAPIC_TOTAL', "Photos envoyées depuis toujours.");
define('TXT_FRAMAPIC_AVERAGE', "Photos envoyées en moyenne chaque jour.");

//---------------------------------------------------------
// Framalibre
//---------------------------------------------------------

define('TXT_FRAMALIBRE_DESCRIPTION', "L’annuaire de logiciels libres testés et commentés dans le projet historique et fondateur de Framasoft.");
define('TXT_FRAMALIBRE_DESCRIPTION_LIEN', " Venez le feuilleter ici : ");
define('TXT_FRAMALIBRE_STAT_ALL_TYPES', "Notices répertoriées en total");
define('TXT_LIBRE_SOFT_COUNT', "Notices de logiciels crées");
define('TXT_LIBRE_FAQ_COUNT', "Notices de foires aux questions crées");
define('TXT_LIBRE_HARDWARE_COUNT', "Notices de matériel crées");
define('TXT_LIBRE_MEDIA_COUNT', "Notices de médias crées");
define('TXT_LIBRE_CHRONIQUE_COUNT', "Notices de chroniques crées");
define('TXT_LIBRE_ARTICLE_COUNT', "Notices d'articles crées");
define('TXT_LIBRE_LIVRE_COUNT', "Notices de livres crées");
define('TXT_LIBRE_OTHER_COUNT', "Notices autres crées");
define('TXT_FRAMALIBRE_STAT_ALL_TYPES_CONTRIB', "Personnes ayant contribuées en total");
define('TXT_LIBRE_SOFT_CONTRIB', "Contributeur·rices pour les logiciels");
define('TXT_LIBRE_FAQ_CONTRIB', "Contributeur·rices pour les foires aux questions");
define('TXT_LIBRE_HARDWARE_CONTRIB', "Contributeur·rices pour le matériel");
define('TXT_LIBRE_MEDIA_CONTRIB', "Contributeur·rices pour les médias");
define('TXT_LIBRE_CHRONIQUE_CONTRIB', "Contributeur·rices pour les chroniques");
define('TXT_LIBRE_ARTICLE_CONTRIB', "Contributeur·rices pour les articles");
define('TXT_LIBRE_LIVRE_CONTRIB', "Contributeur·rices pour les livres");
define('TXT_LIBRE_OTHER_CONTRIB', "Contributeur·rices pour les autres");

//---------------------------------------------------------
// Framablog
//---------------------------------------------------------

define('TXT_FRAMABLOG_DESCRIPTION', "Chroniques quotidiennes autour du Libre en général et du logiciel libre en particulier. Débats, traductions originales et annonces des nouveautés de l’ensemble du réseau Framasoft.");
define('TXT_FRAMABLOG_DESCRIPTION_LIEN', " Découvrez tout ça ici : ");
define('TXT_FRAMABLOG_STAT_POSTS_TITLE', "Articles");
define('TXT_FRAMABLOG_STAT_POSTS_TITLE_TODAY', "Article aujourd'hui ");
define('TXT_FRAMABLOG_STAT_CATEGORY_TITLE', "Catégories");
define('TXT_FRAMABLOG_STAT_CATEGORY_TEXT_1', "Les catégories les plus representées avec le plus d'articles sont ");
define('TXT_FRAMABLOG_STAT_COMMENTS_TITLE', "Commentaires");

//---------------------------------------------------------
// Framasphere
//---------------------------------------------------------

define('TXT_FRAMASPHERE_DESCRIPTION', " est une instance de Diaspora, le réseau social libre et décentralisé."); // Framasphere est une ...
define('TXT_FRAMASPHERE_STAT_TOTAL_USERS', "Utilisateurs");
define('TXT_FRAMASPHERE_STAT_POSTS', "Messages");
define('TXT_FRAMASPHERE_STAT_COMMENTS', "Commentaires");
define('TXT_FRAMASPHERE_ACTIVE_MONTHLY', "Actifs dans le dernier mois");
define('TXT_FRAMASPHERE_ACTIVE_HALFYEAR', "Actifs dans les 6 derniers mois");

//---------------------------------------------------------
// Framabook
//---------------------------------------------------------

define('TXT_FRAMABOOK_DESCRIPTION', "Notre maison d’édition autour du logiciel libre, de l'initiation à la programmation, de la réflexion sur le droit d’auteur mais aussi des BD et même des romans.");
define('TXT_FRAMABOOK_DESCRIPTION_LIEN', " Venez découvrir tout ça ici : ");
define('TXT_FRAMABOOK_STAT_BOOKS', "Livres et autres documents disponibles.");
define('TXT_FRAMABOOK_STAT_ALL_TITLE', "Téléchargements depuis la nuit des temps");
define('TXT_FRAMABOOK_STAT_AVG_STAT', "Téléchargements en moyenne chaque jour");
define('TXT_FRAMABOOK_STAT_AVG_TEXT_1', "Jour le plus prolifique : "); 	// 30/08/1993
define('TXT_FRAMABOOK_STAT_AVG_TEXT_2', "  avec ");				// 69
define('TXT_FRAMABOOK_STAT_AVG_TEXT_3', "  téléchargements.");
define('TXT_FRAMABOOK_STAT_TODAY_STAT', "Téléchargements aujourd'hui.");
define('TXT_FRAMABOOK_STAT_TOP_DOWNLDS', "TOP 10 des téléchargements.");
define('TXT_FRAMABOOK_STAT_TOP_DOWNLDS_W', "TOP 10 des téléchargements dans les 7 derniers jours.");


//---------------------------------------------------------
// Framadvd
//---------------------------------------------------------

define('TXT_FRAMADVD_DESCRIPTION', "Un DVD rempli des meilleurs logiciels libres pour Windows, Mac et GNU/Linux (avec une distribution dedans) mais aussi de la culture libre (textes, photos, audios et vidéos). Existe en une version spéciale école.");
define('TXT_FRAMADVD_DESCRIPTION_LIEN', " Venez le télécharger gratuitement ici");
define('TXT_FRAMADVD_DWNLDS_ALL', "Téléchargements");
define('TXT_FRAMADVD_DWNLDS_ALL_TXT', "Depuis mi-juillet 2015 et le début des statistiques.");
define('TXT_FRAMADVD_DWNLDS_TODAY_CLASSIC', "Téléchargements aujourd'hui du DVD Classique");
define('TXT_FRAMADVD_DWNLDS_TODAY_SCHOOL', "Téléchargements aujourd'hui du DVD Ecole");
define('TXT_FRAMADVD_DWNLDS_TEXT_1', "Jour le plus prolifique : "); 	// 30/08/1993
define('TXT_FRAMADVD_DWNLDS_TEXT_2', "  avec ");				// 69
define('TXT_FRAMADVD_DWNLDS_TEXT_3', "  téléchargements.");

//---------------------------------------------------------
// Framabin
//---------------------------------------------------------

define('TXT_FRAMABIN_DESCRIPTION', "Avec Framabin, vous pouvez partager des informations que seuls vous et votre correspondant aurez le pouvoir de déchiffrer, et ceci de manière très simple.");
define('TXT_FRAMABIN_DESCRIPTION_LIEN', " C'est ici que ça se passe.");
define('TXT_FRAMABIN_DOCS_ALL_TITLE', "Documents crées depuis tout temps.");
define('TXT_FRAMABIN_SHARED_DOCS_ALL_TITLE', "Consultations de documents.");
define('TXT_FRAMABIN_DOCS_TODAY', "Document(s) créé(s) aujourd'hui.");
define('TXT_FRAMABIN_SHARED_DOCS_TODAY', "Consultation(s) aujourd'hui.");
define('TXT_FRAMABIN_DOCS_TODAY_1', "Jour le plus prolifique : "); 	// 30/08/1993
define('TXT_FRAMABIN_DOCS_TODAY_2', "  avec ");				// 69
define('TXT_FRAMABIN_DOCS_TODAY_3', "  documents crées.");
define('TXT_FRAMABIN_SHARED_DOCS_TODAY_1', "Jour le plus prolifique : "); 	// 30/08/1993
define('TXT_FRAMABIN_SHARED_DOCS_TODAY_2', "  avec ");				// 69
define('TXT_FRAMABIN_SHARED_DOCS_TODAY_3', "  consultations de documents.");

//---------------------------------------------------------
// Framaslides
//---------------------------------------------------------

define('TXT_FRAMASLIDES_DESCRIPTION', "Éditez vos présentations");
define('TXT_FRAMASLIDES_DESCRIPTION_LIEN', " en ligne");
define('TXT_FRAMASLIDES_PRESENTATIONS', "Présentations.");
define('TXT_FRAMASLIDES_ENABLED_USERS', "Utilisateurs.");
define('TXT_FRAMASLIDES_GROUPS', "Groupes.");
define('TXT_FRAMASLIDES_TEMPLATES', "Modèles de présentations.");
define('TXT_FRAMASLIDES_GRAPHE', "Évolution du nombre de présentations et utilisateurs.");

//---------------------------------------------------------
// Framapiaf
//---------------------------------------------------------

define('TXT_FRAMAPIAF_DESCRIPTION', "Microblogging");
define('TXT_FRAMAPIAF_DESCRIPTION_LIEN', " libre et fédéré");
define('TXT_FRAMAPIAF_USERS', "Utilisateurs.");
define('TXT_FRAMAPIAF_POSTS', "Posts.");
define('TXT_FRAMAPIAF_CONNECTEDNODS', "Autres instances connectées.");
define('TXT_FRAMAPIAF_GRAPHE', "Évolution du nombre d'utilisateurs et de posts.");

//---------------------------------------------------------
// Réseaux Sociaux
//---------------------------------------------------------

define('TXT_SOCIAL_NETWORKS', "Réseaux Sociaux");
define('TXT_SOCIAL_NETWORKS_DESCRIPTION', "Pour nous suivre rendez-vous sur : "); // ... rendez-vous sur : Diaspora ...
define('TXT_SOCIAL_NETWORKS_DESCRIPTION_LIEN_1', " - le réseau social libre. Nous sommes malheureusement aussi sur "); // ... Twitter, Facebook ...
define('TXT_SOCIAL_NETWORKS_STAT_TWITTER_1', "Abonnés Twitter");
define('TXT_SOCIAL_NETWORKS_STAT_TWITTER_2', "Tweets");
define('TXT_SOCIAL_NETWORKS_STAT_FACEBOOK_1', "Mentions \"J'aime\" sur Facebook");

//---------------------------------------------------------
// Autres statistiques
//---------------------------------------------------------

define('TXT_OTHER_STATS', "Autres statistiques");
define('TXT_OTHER_STATS_RANK_TITLE', "Classement des sites les plus visités");
?>
