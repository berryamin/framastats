<?php
/*
-----------------
Language: English
-----------------
*/

//---------------------------------------------------------
// index.php and other places
//---------------------------------------------------------

define('TXT_DESCRIPTION_SITE', "Public statistics tool for all <a href='http://www.framasoft.org'>Framasoft</a> services.");
define('TXT_SHOULD_NOT_APPEAR', "Error loading stat");
define('TXT_LABEL_CHEVRON_DOWN', "aria-label='Display more statistics'");

define('TXT_DESCRIPTION_LIEN', "Service available here");

define('TXT_FRAMASOFT', "Framasoft");
define('TXT_FRAMASOFT_NETWORK', "Framasoft popular and educational network");
define('TXT_SHOULD_NOT_APPEAR', "TXT_SHOULD_NOT_APPEAR");
define('TXT_LABEL_CHEVRON_DOWN', "aria-label='Display more statistics'");
define('TXT_LABEL_ICON_STAT', "aria-label='Display more statistics and figures'");
define('TXT_NAVIGATION', "Navigation");
define('TXT_UPDATE', "Last update : ");
define('TXT_AND', " and ");

define('TXT_THIS_YEAR', "This year :");
define('TXT_1_LAST_YEAR', "12 last months:");
define('TXT_6_LAST_MONTHS', "6 last months:");
define('TXT_3_LAST_MONTHS', "3 last months:");
define('TXT_1_LAST_MONTH', "30 last days:");
define('TXT_1_LAST_WEEK', "7 last days:");
define('TXT_1_LAST_DAY', "Yesterday :");
define('TXT_1_TODAY', "Today :");
define('TXT_ADD_YEAR', "Addition this year");
define('TXT_ADD_6MONTHS', "Addition the last 6 months");
define('TXT_ADD_3MONTHS', "Addition the last 3 months");
define('TXT_ADD_1MONTH', "Addition in the last month");

define('TXT_DESCRIPTION_LIEN', "Service available here");

//---------------------------------------------------------
// Framasoft
//---------------------------------------------------------

define('TXT_FRAMASOFT_DESCRIPTION', "Framasoft is a popular and educational network dedicated to the Free-Libre software and culture. Since 2014, the non-profit organization tries this crazy challenge to ");
define('TXT_FRAMASOFT_DESCRIPTION_LIEN', "de-google-ify Internet");
define('TXT_FRAMASOFT_YEARS', "Years old for Framasoft network");
define('TXT_FRAMASOFT_YEARS_ORG_1', "And "); // Et .. 11 .. années d'existence
define('TXT_FRAMASOFT_YEARS_ORG_2', " years old for the non-profit organization (2004).");
define('TXT_FRAMASOFT_COUNT_SERVICES', "Actives projects");
define('TXT_FRAMASOFT_IN_PROGRESS',  "Projects in pending");
define('TXT_FRAMASOFT_VISIT_ALL_TITLE', "Visits since universe beginning");
define('TXT_FRAMASOFT_VISIT_ALL_TEXT_1',  " \"universe beginning\" = since 2013 when we installed ");
define('TXT_FRAMASOFT_STAT_AVG_STAT', "Daily visits");
define('TXT_FRAMASOFT_STAT_AVG_TXT_1', "Most prolific day : "); 	// 30/08/1993
define('TXT_FRAMASOFT_STAT_AVG_TXT_2', "  with ");				// 45000
define('TXT_FRAMASOFT_STAT_AVG_TXT_3', "  visits.");
define('TXT_FRAMASOFT_VISIT_TODAY_TITLE', "Visits today");
define('TXT_FRAMASOFT_MEMBERS',  "Members");
define('TXT_FRAMASOFT_VOLUNTARY_WORK',  "Hours of valued volunteerism");
define('TXT_FRAMASOFT_DONATION',  "Euros for donations");
define('TXT_FRAMASOFT_REGULAR_DONATION_1',  "With "); // Dont 1200 € de dons récurrents
define('TXT_FRAMASOFT_REGULAR_DONATION_2',  "€ of recurring donations.");


//---------------------------------------------------------
// MyFrama
//---------------------------------------------------------

define('TXT_MYFRAMA_DESCRIPTION', "Mark and share your links.");
define('TXT_MYFRAMA_DESCRIPTION_LIEN', "And hop, it happens here.");
define('TXT_MYFRAMA_ACCOUNT_NB', "Created accounts.");

//---------------------------------------------------------
// Framadate
//---------------------------------------------------------


define('TXT_FRAMADATE_DESCRIPTION', "Framadate c’est un peu comme Doodle mais en libre. Rien de plus simple de créer un rendez-vous ou un sondage sans inscription pour y inviter vos collaborateurs.");
define('TXT_FRAMADATE_DESCRIPTION_LIEN', " Ca se passe ici : ");
define('TXT_FRAMADATE_STAT_ALL_TITLE', "Pulls created since time immemorial");
define('TXT_FRAMADATE_STAT_AVG_STAT', "Sondages crées en moyenne chaque jour");
define('TXT_FRAMADATE_STAT_AVG_TEXT_1', "Jour le plus prolifique : "); 	// 30/08/1993
define('TXT_FRAMADATE_STAT_AVG_TEXT_2', "  avec ");				// 69
define('TXT_FRAMADATE_STAT_AVG_TEXT_3', "  sondages créés.");
define('TXT_FRAMADATE_STAT_TODAY_STAT', "Sondages créés aujourd'hui");
define('TXT_FRAMADATE_STAT_LIFE_EXPECTANCY', "Durée de vie d'un sondage moyen (en jours)");
define('TXT_FRAMADATE_STAT_AVG_USERS', "Utilisateurs moyens par sondages");
define('TXT_FRAMADATE_STAT_FORMAT_TITLE', "% de sondages de type 'Date'");
define('TXT_FRAMADATE_STAT_FORMAT_1', "Sondages 'Classique'");
define('TXT_FRAMADATE_STAT_FORMAT_2', "Sondages 'Date' sans modifications possibles des utilisateurs");
define('TXT_FRAMADATE_STAT_FORMAT_3', "Sondages 'Classique' sans modifications possibles des utilisateurs");
define('TXT_FRAMADATE_STAT_FORMAT_TXT', "Le reste étant de vieux types de sondages tout démodés.");

//---------------------------------------------------------
// Framapad
//---------------------------------------------------------

define('TXT_FRAMAPAD_DESCRIPTION', "Écrivons tous ensemble sur le même document dans une interface de rédaction collective en ligne. Aucune inscription requise, on se donne un pseudo, une couleur et on se lance !");
define('TXT_FRAMAPAD_DESCRIPTION_LIEN', " Ca se passe ici : ");
define('TXT_FRAMAPAD_STAT_ALL_TITLE', "Pads actuellement actifs");
define('TXT_FRAMAPAD_STAT_ALL_TEXT', "Statistiques concernant les pads créés depuis la mise en place en 24 mars 2015 des pads temporaires et illimités.");
define('TXT_PADS_DAILY', "Pads quotidiens :");
define('TXT_PADS_WEEKLY', "Pads hebdomadaires :");
define('TXT_PADS_MONTHLY', "Pads mensuels :");
define('TXT_PADS_BIMONTHLY', "Pads bimensuels :");
define('TXT_PADS_BIANNUAL', "Pads semestriels :");
define('TXT_PADS_ANNUAL', "Pads annuels :");
define('TXT_PADS_ETERNAL', "Pads éternels :");
define('TXT_FRAMAPAD_STAT_BLANK', "% de pads vides");

//---------------------------------------------------------
// Framalibre
//---------------------------------------------------------

define('TXT_FRAMALIBRE_DESCRIPTION', "L’annuaire de logiciels libres testés et commentés dans le projet historique et fondateur de Framasoft.");
define('TXT_FRAMALIBRE_DESCRIPTION_LIEN', " Venez le feuilleter ici : ");
define('TXT_FRAMALIBRE_STAT_ALL_TYPES', "Notices répertoriées en total");
define('TXT_LIBRE_SOFT_COUNT', "Notices de logiciels crées");
define('TXT_LIBRE_FAQ_COUNT', "Notices de foires aux questions crées");
define('TXT_LIBRE_HARDWARE_COUNT', "Notices de matériel crées");
define('TXT_LIBRE_MEDIA_COUNT', "Notices de médias crées");
define('TXT_LIBRE_CHRONIQUE_COUNT', "Notices de chroniques crées");
define('TXT_LIBRE_ARTICLE_COUNT', "Notices d'articles crées");
define('TXT_LIBRE_LIVRE_COUNT', "Notices de livres crées");
define('TXT_LIBRE_OTHER_COUNT', "Notices autres crées");
define('TXT_FRAMALIBRE_STAT_ALL_TYPES_CONTRIB', "Personnes ayant contribuées en total");
define('TXT_LIBRE_SOFT_CONTRIB', "Contributeur·rice pour les logiciels");
define('TXT_LIBRE_FAQ_CONTRIB', "Contributeur·rice pour les foires aux questions");
define('TXT_LIBRE_HARDWARE_CONTRIB', "Contributeur·rice pour le matériel");
define('TXT_LIBRE_MEDIA_CONTRIB', "Contributeur·rice pour les médias");
define('TXT_LIBRE_CHRONIQUE_CONTRIB', "Contributeur·rice pour les chroniques");
define('TXT_LIBRE_ARTICLE_CONTRIB', "Contributeur·rice pour les articles");
define('TXT_LIBRE_LIVRE_CONTRIB', "Contributeur·rice pour les livres");
define('TXT_LIBRE_OTHER_CONTRIB', "Contributeur·rice pour les autres");

//---------------------------------------------------------
// Framablog
//---------------------------------------------------------

define('TXT_FRAMABLOG_DESCRIPTION', "Chroniques quotidiennes autour du Libre en général et du logiciel libre en particulier. Débats, traductions originales et annonces des nouveautés de l’ensemble du réseau Framasoft.");
define('TXT_FRAMABLOG_DESCRIPTION_LIEN', " Découvrez tout ça ici : ");
define('TXT_FRAMABLOG_STAT_POSTS_TITLE', "Articles");
define('TXT_FRAMABLOG_STAT_POSTS_TITLE_TODAY', "Article aujourd'hui ");
define('TXT_FRAMABLOG_STAT_CATEGORY_TITLE', "Catégories");
define('TXT_FRAMABLOG_STAT_CATEGORY_TEXT_1', "Les catégories les plus representées avec le plus d'articles sont ");
define('TXT_FRAMABLOG_STAT_COMMENTS_TITLE', "Commentaires");

//---------------------------------------------------------
// Framasphere
//---------------------------------------------------------

define('TXT_FRAMASPHERE_DESCRIPTION', " est une instance de Diaspora, le réseau social libre et décentralisé."); // Framasphere est une ...
define('TXT_FRAMASPHERE_STAT_TOTAL_USERS', "Utilisateurs");
define('TXT_FRAMASPHERE_STAT_POSTS', "Messages");
define('TXT_FRAMASPHERE_STAT_COMMENTS', "Commentaires");
define('TXT_FRAMASPHERE_ACTIVE_MONTHLY', "Actifs dans le dernier mois");
define('TXT_FRAMASPHERE_ACTIVE_HALFYEAR', "Actifs dans les 6 derniers mois");

//---------------------------------------------------------
// Framabook
//---------------------------------------------------------

define('TXT_FRAMABOOK_DESCRIPTION', "Notre maison d’édition autour du logiciel libre, de l'initiation à la programmation, de la réflexion sur le droit d’auteur mais aussi des BD et même des romans.");
define('TXT_FRAMABOOK_DESCRIPTION_LIEN', " Venez découvrir tout ça ici : ");
define('TXT_FRAMABOOK_STAT_BOOKS', "Livres et autres documents disponibles.");
define('TXT_FRAMABOOK_STAT_ALL_TITLE', "Téléchargements depuis la nuit des temps");
define('TXT_FRAMABOOK_STAT_AVG_STAT', "Téléchargements en moyenne chaque jour");
define('TXT_FRAMABOOK_STAT_AVG_TEXT_1', "Jour le plus prolifique : "); 	// 30/08/1993
define('TXT_FRAMABOOK_STAT_AVG_TEXT_2', "  avec ");				// 69
define('TXT_FRAMABOOK_STAT_AVG_TEXT_3', "  téléchargements.");
define('TXT_FRAMABOOK_STAT_TODAY_STAT', "Téléchargements aujourd'hui.");
define('TXT_FRAMABOOK_STAT_TOP_DOWNLDS', "TOP 10 des téléchargements.");
define('TXT_FRAMABOOK_STAT_TOP_DOWNLDS_W', "TOP 10 des téléchargements dans les 7 derniers jours.");


//---------------------------------------------------------
// Framadvd
//---------------------------------------------------------

define('TXT_FRAMADVD_DESCRIPTION', "Un DVD rempli des meilleurs logiciels libres pour Windows, Mac et GNU/Linux (avec une distribution dedans) mais aussi de la culture libre (textes, photos, audios et vidéos). Existe en une version spéciale école.");
define('TXT_FRAMADVD_DESCRIPTION_LIEN', " Venez le télécharger gratuitement ici");
define('TXT_FRAMADVD_DWNLDS_ALL', "Téléchargements");
define('TXT_FRAMADVD_DWNLDS_ALL_TXT', "Depuis mi-juillet 2015 et le début des statistiques.");
define('TXT_FRAMADVD_DWNLDS_TODAY_CLASSIC', "Téléchargements aujourd'hui du DVD Classique");
define('TXT_FRAMADVD_DWNLDS_TODAY_SCHOOL', "Téléchargements aujourd'hui du DVD Ecole");
define('TXT_FRAMADVD_DWNLDS_TEXT_1', "Jour le plus prolifique : "); 	// 30/08/1993
define('TXT_FRAMADVD_DWNLDS_TEXT_2', "  avec ");				// 69
define('TXT_FRAMADVD_DWNLDS_TEXT_3', "  téléchargements.");

//---------------------------------------------------------
// Framabin
//---------------------------------------------------------

define('TXT_FRAMABIN_DESCRIPTION', "Avec Framabin, vous pouvez partager des informations que seuls vous et votre correspondant aurez le pouvoir de déchiffrer, et ceci de manière très simple.");
define('TXT_FRAMABIN_DESCRIPTION_LIEN', " C'est ici que ça se passe.");
define('TXT_FRAMABIN_DOCS_ALL_TITLE', "Documents crées depuis tout temps.");
define('TXT_FRAMABIN_SHARED_DOCS_ALL_TITLE', "Consultations de documents.");
define('TXT_FRAMABIN_DOCS_TODAY', "Document(s) créé(s) aujourd'hui.");
define('TXT_FRAMABIN_SHARED_DOCS_TODAY', "Consultation(s) aujourd'hui.");
define('TXT_FRAMABIN_DOCS_TODAY_1', "Jour le plus prolifique : "); 	// 30/08/1993
define('TXT_FRAMABIN_DOCS_TODAY_2', "  avec ");				// 69
define('TXT_FRAMABIN_DOCS_TODAY_3', "  documents crées.");
define('TXT_FRAMABIN_SHARED_DOCS_TODAY_1', "Jour le plus prolifique : "); 	// 30/08/1993
define('TXT_FRAMABIN_SHARED_DOCS_TODAY_2', "  avec ");				// 69
define('TXT_FRAMABIN_SHARED_DOCS_TODAY_3', "  consultations de documents.");

//---------------------------------------------------------
// Framaslides
//---------------------------------------------------------

define('TXT_FRAMASLIDES_DESCRIPTION', "Éditez vos présentations");
define('TXT_FRAMASLIDES_DESCRIPTION_LIEN', " en ligne");
define('TXT_FRAMASLIDES_PRESENTATIONS', "Présentations.");
define('TXT_FRAMASLIDES_ENABLED_USERS', "Utilisateurs.");
define('TXT_FRAMASLIDES_GROUPS', "Groupes.");
define('TXT_FRAMASLIDES_TEMPLATES', "Modèles de présentations.");
define('TXT_FRAMASLIDES_GRAPHE', "Évolution du nombre de présentations et utilisateurs.");

//---------------------------------------------------------
// Framapiaf
//---------------------------------------------------------

define('TXT_FRAMAPIAF_DESCRIPTION', "Microblogging");
define('TXT_FRAMAPIAF_DESCRIPTION_LIEN', " libre et fédéré");
define('TXT_FRAMAPIAF_USERS', "Utilisateurs.");
define('TXT_FRAMAPIAF_POSTS', "Posts.");
define('TXT_FRAMAPIAF_CONNECTEDNODS', "Autres instances connectées.");
define('TXT_FRAMAPIAF_GRAPHE', "Évolution du nombre d'utilisateurs et de posts.");

//---------------------------------------------------------
// Réseaux Sociaux
//---------------------------------------------------------

define('TXT_SOCIAL_NETWORKS', "Réseaux Sociaux");
define('TXT_SOCIAL_NETWORKS_DESCRIPTION', "Pour nous suivre rendez-vous sur : "); // ... rendez-vous sur : Diaspora ...
define('TXT_SOCIAL_NETWORKS_DESCRIPTION_LIEN_1', " - le réseau social libre. Nous sommes malheureusement aussi sur "); // ... Twitter, Facebook ...
define('TXT_SOCIAL_NETWORKS_DESCRIPTION_LIEN_2', " et même "); // ... Google+
define('TXT_SOCIAL_NETWORKS_STAT_TWITTER_1', "Abonnés Twitter");
define('TXT_SOCIAL_NETWORKS_STAT_TWITTER_2', "Tweets");
define('TXT_SOCIAL_NETWORKS_STAT_FACEBOOK_1', "Mentions \"J'aime\" sur Facebook");

//---------------------------------------------------------
// Autres statistiques
//---------------------------------------------------------

define('TXT_OTHER_STATS', "Autres statistiques");
define('TXT_OTHER_STATS_RANK_TITLE', "Classement des sites les plus visités");
?>
