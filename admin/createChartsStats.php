<?php
/*
 * TO BE REMOVED IF NOT USED
 *
 * Create all historic stats that will be useful for charts in framastats.org
 * Used stats recorded in framastats database
 * 
 * */

/*
// ################# START TIME ###################
$timeStart	= microtime(true);

// ################## INCLUDE #####################
$pathToUtil_mySQL 	= '../scripts/mySQL/Util_mySQL.php';
$pathToUtil		= '../scripts/utils/Util.php';
include ($pathToUtil_mySQL);
include ($pathToUtil);

// ############### UTILS & DEBUG ##################
$util = new Util();
$util->setDebug("error");

// ################# CONSTANTS ####################
$nbOfArguments		= 1;
$nbOfArgumentsDebug	= $nbOfArguments + 1;
$error			= "### ERROR ###";
$man			= "You should call script as follows :" . PHP_EOL .
			  "> php createChartsStats.php (debug)";
$scriptEnds 		= "### SCRIPT ENDS ###" ;

$urlChartsStats		= 'http://framastats.org/autresStats/chartsStats.json';
$pathDbInfos		= '../../dbInfos_Framastats.json';

$defaultInfo		= 'TO_CHANGE';
$tableFramastats_hstry	= 'framastats_history';
$tableFramastat_srvc	= 'framastats_service';
$columnService 		= 'service';
$columnNameStat 	= 'nameStat';
$columnValueStat 	= 'valueStat';
$columnDate 		= 'date';

// #################### DATABASE ##################
$dbInfos = array (
	'db_host' 	=> $defaultInfo,
	'db_dbname' 	=> $defaultInfo,
	'db_usr' 	=> $defaultInfo,
	'db_pswrd' 	=> $defaultInfo
	);

// ############## FUNCTIONS ###################

// ########## DATABASE CONNECTION #############
$util->out("### Create Charts Stats", "info");

$mode = null;

// Check number of arguments
if ($argc < $nbOfArguments || $argc > $nbOfArgumentsDebug) {
	$util->out($error . PHP_EOL . $man . PHP_EOL . $scriptEnds, "error", true); // exit
}
// Debug And Init Mode
elseif ($argc == $nbOfArgumentsDebug) {
	$mode = $argv[$nbOfArgumentsDebug-1];
	if ($mode == "debug") {
		$util->setDebug("all");
		$modeUpperCase =  mb_strtoupper($mode);
		$util->out("### $modeUpperCase MODE", "info");
	} else {
		$util->out($error . PHP_EOL . $man . PHP_EOL . $scriptEnds, "error", true); // exit
	}
}

// Manage database informations
$finalDbInfos = $util->checkVariablesOrSetThem($pathDbInfos, $dbInfos, $defaultInfo);

// Database connection
try
{
	$dBase = new PDO('mysql:host=' . $finalDbInfos['db_host'] . ';dbname='. $finalDbInfos['db_dbname'] . ';charset=utf8', $finalDbInfos['db_usr'], $finalDbInfos['db_pswrd']);
	$util->out("### Established connection in the database : " . $finalDbInfos['db_dbname'], "success");
}
catch(Exception $e)
{
	$util->out("### Error in database connection : ".$e->getMessage(), "error");
	$util->out("### Check infos in this file : ". $pathDbInfos, "error", true);
}

// ################# REQUESTS ###################

// *** Daily visits
$dailyVisitStat = 'rest_json_nbVisits_today_total';
$dateFirstStat	= '2015-07-27';
$res = $dBase->query("SELECT DATE($columnDate) as day, MAX(CAST($columnValueStat AS UNSIGNED)) as max
			FROM `$tableFramastats_hstry`
			WHERE `$columnService` = 'framastats' 
			AND `$columnNameStat` = '$dailyVisitStat'
			AND $columnDate >= '$dateFirstStat'
			GROUP BY day");

while ($data = $res->fetch())
{
	$array_DailyVisits_day[] = $data['day'];
	$array_DailyVisits_value[] = $data['max'];
}
$res->closeCursor();

// ################# END TIME ###################
$timeEnd 	= microtime(true);
$time		= $timeEnd - $timeStart;
$util->out("### Request took $time seconds", 'info');
*/
?>
