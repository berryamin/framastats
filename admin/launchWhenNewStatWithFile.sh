#!/bin/bash
#!/usr/bin/php
#
#
# But :     intégrer de nouvelles stats facilement sur framastats.org
#           grâce à un fichier json
# Comment : Lancer ce script avec le nom du nouveau service,
#	    nom correspondant au nom du fichier de stats
#

# Constantes
#
PATHTOINDEX='../web/index.php';
PATHTOPATTERN_SON='../web/scripts/php/patternDivForOnesValues_son.php';
PATHTOPATTERN_FATHER='../web/scripts/php/patternDivForOnesValues_father.php';
PATHTOSTATS='../../otherStats';
PATHTOHANDLESTATS='../../../../web/scripts/php/';
TO_REPLACE='<!-- TO REPLACE WITH STAT ->';
TO_REPLACE_INDEX='<!-- NEXT STATS -->';
TO_REPLACE_LINK='<!-- NEXT LINK -->';
TO_REPLACE_STAT_NAME=' <!-- REPLACE_ME_WITH_STAT_NAME --> ';
TO_REPLACE_DESCRIPTION=' <!-- REPLACE_WITH_DESCRIPTION --> ';
name_to_replace='framaxxx';
Name_to_replace='Framaxxx';
NAME_TO_REPLACE='FRAMAXXX';

TMP_STATS='/tmp/framastats_stats'
LIMITER="==>"
REGEX="(.*)$LIMITER(.*)"

TMP_FORINDEX='/tmp/framastats_forindex.php'
TMP_INDEX='/tmp/framastats_index.php'

# Vérification du nombre d'argument
#
if [ $# -ne 1 ]
then
   echo "?? usage : $0 <nom nouveau service>" >&2
   exit 1
fi

# Création variables avec différents casses (utile aux remplacements futurs du pattern)
#
_SERVICE_NAME=$1
service_name=$(echo ${1,,})
Service_name=$(echo "${1^}")
SERVICE_NAME=$(echo "${1^^}")

# Vérification de l'existence du fichier de stats
#
if [ -f "$PATHTOSTATS/$service_name.json" ]; then
	pathToStats="$PATHTOSTATS/$service_name.json";
elif [ -f "$PATHTOSTATS/$Service_name.json" ]; then
	pathToStats="$PATHTOSTATS/$Service_name.json";
elif [ -f "$PATHTOSTATS/$SERVICE_NAME.json" ]; then
	pathToStats="$PATHTOSTATS/$SERVICE_NAME.json";
else
	echo -e "?? Je n'ai pas trouvé le fichier de stats ! \nIl doit s'appeller $1.json et être situé ici $PATHTOSTATS" >&2
	exit 1
fi

# 1. Change pattern père avec le nom du nouveau service
TMP_FATHER='/tmp/framastats_tmp_father'
cp $PATHTOPATTERN_FATHER $TMP_FATHER
sed -i -e "s/$name_to_replace/$service_name/g" $TMP_FATHER
sed -i -e "s/$Name_to_replace/$Service_name/g" $TMP_FATHER
sed -i -e "s/$NAME_TO_REPLACE/$SERVICE_NAME/g" $TMP_FATHER

# 2. Change pattern fils avec le nom du nouveau service
TMP_SON='/tmp/framastats_tmp_son'
cp $PATHTOPATTERN_SON $TMP_SON
sed -i -e "s/$name_to_replace/$service_name/g" $TMP_SON
sed -i -e "s/$Name_to_replace/$Service_name/g" $TMP_SON
sed -i -e "s/$NAME_TO_REPLACE/$SERVICE_NAME/g" $TMP_SON

# 3. Gestion du nvo fichier de stats
# ----------------------------------
# 3.01 Remplace tous les guillemets " car ça pose des soucis avec sed
sed -i -e 's/"/<replaced>/g' $TMP_SON
# 3.02 On récupère le pattern fils
son=$(<$TMP_SON)

# 3.1 Iteration sur le fichier de stats, création de TMP_STATS traitable en bash
jq -r "to_entries|map(\"\(.key)$LIMITER\(.value|tostring)\")|.[]" $pathToStats > $TMP_STATS

# 3.2 On parcourt chaque stats et on ajoute un pattern fils à chaque fois
while read line; do
    if [[ $line =~ $REGEX ]]; then
        name="${BASH_REMATCH[1]}"
        value="${BASH_REMATCH[2]}"
        if [[ "$name" != "serviceName" ]] ; then
		# 3.3 replace zone to remplace with son
		fatherWithSon=$(awk "/$TO_REPLACE/ { print \"$son \n$TO_REPLACE \"; next }1" $TMP_FATHER)
		# 3.4 write res in tmp file
		echo "$fatherWithSon" > $TMP_FATHER
		# 3.5 replace class name with stat name
		classNameToReplace=$service_name"_stat";
		statName=$service_name"_"$name
		sed -i -e "s/$classNameToReplace/$statName/g" $TMP_FATHER
		# 3.6 Remplace le nom de la stat
		statNameWithoutUnderscore=${name//_/ };
		Stat_name=$(echo "${statNameWithoutUnderscore^}");
		sed -i -e "s/$TO_REPLACE_STAT_NAME/$Stat_name/g" $TMP_FATHER;
		# 3.7 Affichage debug
		echo -e ">> Ajout de la stat '$Stat_name'"
        fi
    fi
done < $TMP_STATS

# 4.0 Description du service
# --------------------------
# 4.1 Demande la description
echo -e "\e[1m?? Donner la description du service $serviceName : \e[0m"
read description

# 4.2 L'intégrer au pattern
sed -i -e "s/$TO_REPLACE_DESCRIPTION/$description/g" $TMP_FATHER;

# 5.0 Préparer fichier index
# --------------------------
# 5.1 Prépare le pattern père en enlevant les lignes (nécessaires sinon erreur)
toCopyInIndex=$(tr -d '\n' < $TMP_FATHER > $TMP_FORINDEX)
# 5.11 replace all " because it causes problems with sed
sed -i -e 's/"/<replaced>/g' $TMP_FORINDEX
# 5.12 get result
forIndex=$(<$TMP_FORINDEX)

# 5.2 Add all this new stats to index.php > put in tmp file
newIndexPhp=$(awk "/$TO_REPLACE_INDEX/ { print \"$forIndex \n$TO_REPLACE_INDEX \"; next }1" $PATHTOINDEX)
echo "$newIndexPhp" > $TMP_INDEX

# 5.3 Add new link in nav-tab
newLink="<li><a href='#$Service_name'>$Service_name</a></li>";
newNewIndexPhp=$(awk "/$TO_REPLACE_LINK/ { print \"$newLink \n$TO_REPLACE_LINK \"; next }1" $TMP_INDEX)
echo "$newNewIndexPhp" > $TMP_INDEX

# 5.3 replace all <replaced> with "
sed -i -e 's/<replaced>/"/g' $TMP_INDEX;

# 5.4 Replace index.php with our new index.php
cp $TMP_INDEX $PATHTOINDEX

# 6.0 Enregistrement
# ------------------
echo -e "\e[1m>> On enregistre une 1ère fois ces stats (enrgstrmnt automatique par la suite).";
echo -e "\e[1m>> Ceci dure environ 30 secondes.";
php recordAllStats.php
echo -e "\e[1m>> Désolé pour l'attente";
echo -e "\e[1m>> On va préparer le terrain sur framastats.org en récupérant en tâche de fond ces stats";
( cd $PATHTOHANDLESTATS && php handleStats.php &);

# 7.0 Déploiement en ligne
# ------------------------
echo -e "\e[1m?? Prêt à faire apparaître ces stats sur le site et déployer les modifs en ligne (oui/non) ? \e[0m"
answer=""
while [ "$answer" != "oui" ] && [ "$answer" != "non" ] && [ "$answer" == "" ]
do
	read answer
	if [ "$answer" == "oui" ] || [ "$answer" == "non" ]
        then
		break;
	else
        	echo -e "\e[31;1m>> Répondre 'oui' ou 'non' : \e[0m""\c"
		answer=""
		fi
done

case $answer in
	"oui" )
		php deployWeb.php &&
		echo -e "\e[32;1m>> C'est fait, rendez-vous framastats.org !!\e[0m";;
    	"non" )
		echo -e "\e[33;1m>>Ok, il faudra lancer 'php deployWeb.php' à la main.\e[0m";;
esac

exit 0
