<?php

/*
 * include by mySQL/mainScript.php
 *
 * Create $stats
 *
 * */

// ################### CONSTANTS ##################
$nameTable_Dwnlds_log 	= 'wp_download_monitor_log';
$nameTable_Dwnlds_stats	= 'wp_download_monitor_stats';
$nameTable_files	= 'wp_download_monitor_files';

$columnDate_Downlds	= 'date';
//$columnDate_Books	= 'postDate';

// ################### REQUESTS ##################
// *** Nb of books
$res = $dBase->query("SELECT COUNT(*) FROM $nameTable_files");
$nbBooks = $res->fetch();

$res->closeCursor();

// *** Top downloads
$res = $dBase->query("SELECT title, hits
			FROM $nameTable_files
			ORDER BY hits DESC
			LIMIT 10");
while ($data = $res->fetch())
{
	$tmpArray = array($data['title'] => $data['hits']);
	$topDownloads[] = $tmpArray;
}
$res->closeCursor();

// *** Top downloads this week (we can change INTERVAL)
/* $res = $dBase->query("SELECT st.download_id, fi.title as title, SUM(st.hits) as nb_Downloads
* echo "SELECT st.download_id, fi.title as title, SUM(st.hits) as nb_Downloads FROM $nameTable_Dwnlds_stats st INNER JOIN $nameTable_files fi ON st.download_id = fi.id WHERE DATE_SUB(CURDATE(),INTERVAL 1 WEEK) <= date GROUP BY download_id ORDER BY nb_Downloads DESC LIMIT 10";
* $data = $res->fetch();

while ($data = $res->fetch())
{
	$tmpArray = array($data['title'] => $data['nb_Downloads']);
	$topDownloadsThisWeek[] = $tmpArray;
}

print_r($topDownloadsThisWeek);
$res->closeCursor();
*/
// *** Downloads all time
$res = $dBase->query("SELECT SUM(`hits`) FROM $nameTable_files");
$nbDwnlds_all =  $res->fetch();
$res->closeCursor();

// *** Downloads per day since X days -- WE DON'T USE IT
$res = $dBase->query("SELECT COUNT(*) as nb_Downloads, DATE_FORMAT(date, '%Y-%m-%d') as day
			FROM $nameTable_Dwnlds_log
			GROUP BY day
			ORDER BY day DESC
			LIMIT 365");
while ($data = $res->fetch())
{
	$downloadsPerDay_SinceXDays[] = $data['nb_Downloads'];
}
$res->closeCursor();

// ##################### STATS ####################
$stats->mySQL['timeUpdateStats']	= date('Y-m-d H:i:s');

// *** Nb of books
$stats->mySQL['nbBooks'] 		= (int) $nbBooks['COUNT(*)'];

// *** Top downloads
$stats->mySQL['topDownloads'] 		= $topDownloads;
// *** Top downloads this week
//$stats->mySQL['topDownloadsThisWeek'] 	= $topDownloadsThisWeek;

// *** Downloads all time
$stats->mySQL['nbDwnlds_all'] 		= (int) $nbDwnlds_all['SUM(`hits`)'];
$stats->mySQL['nbDwnlds_1lastYear']	= Util_mySQL::countSinceXDays($dBase, $nameTable_Dwnlds_log, $columnDate_Downlds, '1 YEAR');
$stats->mySQL['nbDwnlds_6lastMonths']	= Util_mySQL::countSinceXDays($dBase, $nameTable_Dwnlds_log, $columnDate_Downlds, '6 MONTH');
$stats->mySQL['nbDwnlds_3lastMonths']	= Util_mySQL::countSinceXDays($dBase, $nameTable_Dwnlds_log, $columnDate_Downlds, '3 MONTH');
$stats->mySQL['nbDwnlds_1lastMonth']	= Util_mySQL::countSinceXDays($dBase, $nameTable_Dwnlds_log, $columnDate_Downlds, '1 MONTH');
$stats->mySQL['nbDwnlds_1lastWeek']	= Util_mySQL::countSinceXDays($dBase, $nameTable_Dwnlds_log, $columnDate_Downlds, '1 WEEK');
$stats->mySQL['nbDwnlds_today']		= Util_mySQL::countSinceXDays($dBase, $nameTable_Dwnlds_log, $columnDate_Downlds, '0 DAY');

// *** Downloads per day since X days
$stats->mySQL['downloadsPerDay_SinceXDays'] 	= "DEACTIVATED"//$downloadsPerDay_SinceXDays;

?>
