<?php

/*
 * include by /main.php
 *
 * Handle with connection to Piwik API
 * Redirect request according to the service.
 * Retrieve $stats
 *
 * */

// ################# CONSTANTS ####################
$idSite	= 31; // corresponding to framapic.org

// ################### SCRIPT #####################
$util->out("### Wait for Piwik API to respond...");
$time_start	= microtime(true);

Util_piwik::addVisitsStats($stats, $idSite, $token_auth, $url, $util);

$time_end 	= microtime(true);
$time		= $time_end - $time_start;
$util->out("### Request took $time seconds");

?>
