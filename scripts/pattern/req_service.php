<?php

// ################################################
// ############## THIS IS A PATTERN ###############
// ########## THAT CAN BE USED AND COPY ###########
// ################################################
/*
 * include by pattern/mainScript.php
 *
 * Create $stats
 *
 * */

// ################### CONSTANTS ##################

// ################### REQUESTS ##################

// ##################### STATS ####################
$stats->nameModule['site'] 			= "NAME_OF_SERVICE";
$stats->nameModule['timeUpdateStats']		= date('Y-m-d H:i:s');

?>
