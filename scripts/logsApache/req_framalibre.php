<?php

/*
 * include by logsApache/mainScript.php
 * 
 * Create $stats
 * 
 * */

// ################## CONSTANTS ###################
$totalVisits_start		= 0;
$statusCode 			= 200;
$minBytesDownload		= 0;
$referers	 		= array ("");

$regex_dayOfLastLog 		= "/[0-9]{2}.\/[a-zA-Z]+.\/[0-9]{4}/";
$regex_visit 			= "/^.*GET \/ /";
$regex_control 			= "/ \/ [A-Z]*\/([0-9]*).?([0-9]*)\" ([0-9]*) ([0-9]*) (\".*\") (\".*\")/";
				// capturing group 3 ==> status code HTTP
				// capturing group 4 ==> bytes Downloaded
				// capturing group 5 ==> referer
				// capturing group 6 ==> client browser

// ################### SCRIPT #####################

// Get all lines of downloading
$a_logsVisits 		= preg_grep($regex_visit, $a_logs);

// Delete false positive visits
$numberOfVisits 	= count($a_logsVisits); 
$a_logsVisits 		= Util_logsApache::removeFalsePositive($a_logsVisits, $regex_control, $statusCode, $minBytesDownload);
$finalNumberOfVisits 	= count($a_logsVisits); 

// Display for debug
$nbOfVisitsDeleted = $numberOfVisits - $finalNumberOfVisits;
$util->out("### Remove $nbOfVisitsDeleted visits after checking", "info");
$util->out("### Final number of visits : $finalNumberOfVisits", "info");

// Retrieve (in the log) day of logs
preg_match($regex_dayOfLastLog, json_encode($a_logs), $matches);
$logDate = $matches[0];

// Create stats
$stats = Util_logsApache::dealWithStats($stats, $tmp_pathFileName, $totalVisits_start, $finalNumberOfVisits, $logDate);
?>
