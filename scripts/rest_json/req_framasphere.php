<?php

/*
 * include by rest_json/mainScript.php
 *
 * Create $stats
 *
 * */

// ################### CONSTANTS ##################
$a_urlStat = 'https://framasphere.org/statistics';
$toFindStat	= 'statistic';
$TOTAL_USERS	= 'Total users';
$TOTAL_POSTS	= 'Local posts';
$TOTAL_COMMENTS	= 'Local comments';

// ##################### STATS ####################
$stats->rest_json['site']                       = "Framasphere";
$stats->rest_json['timeUpdateStats']            = date('Y-m-d H:i:s');

// ################ REQUESTS AND STATS ############

if (!file_exists($tmp_pathFileName)) {
        $util->out("### (req_framasphere) Temporary stats file has never been created. ", "info");
}

// Retrieve html file
$doc = new DOMDocument;
libxml_use_internal_errors(true);
$doc->loadHTMLFile($a_urlStat);
libxml_clear_errors();

// Get all h1
$h1s = $doc->getElementsByTagName('h1');
$h1 = $h1s[0];

// Circulate in siblings (corresponding to stats)
$child = $h1->nextSibling;

while ($child) {
        $val = $child->nodeValue;
	// ACTIVATE to see the node names
	// print_r($val);

        if (strpos($val, $TOTAL_USERS) !== false) {
                if ( preg_match('/([0-9]+)/', $val, $match, PREG_OFFSET_CAPTURE) ) {
                // replace comas and spaces  with nothing
                        $strToNb  = str_replace(',','',$match[0][0]);
                        $strToNb2 = str_replace(' ','',$strToNb);
                        //echo "RESULT : $strToNb2";
                        $res[0] = $strToNb2;
               }

        } else if (strpos($val, $TOTAL_POSTS) !== false) {
                if ( preg_match('/([0-9]+)/', $val, $match, PREG_OFFSET_CAPTURE) ) {
                // replace comas and spaces  with nothing
                        $strToNb  = str_replace(',','',$match[0][0]);
                        $strToNb2 = str_replace(' ','',$strToNb);
                        //echo "RESULT : $strToNb2";
                        $res[1] = $strToNb2;
               }

        } else if (strpos($val, $TOTAL_COMMENTS) !== false) {
                if ( preg_match('/([0-9]+)/', $val, $match, PREG_OFFSET_CAPTURE) ) {
                // replace comas and spaces  with nothing
                        $strToNb  = str_replace(',','',$match[0][0]);
                        $strToNb2 = str_replace(' ','',$strToNb);
                        //echo "RESULT : $strToNb2";
                        $res[2] = $strToNb2;
               }

        }
        $child = $child->nextSibling;
}

// ##################### STATS ####################
$stats->rest_json['total_users']      = $res[0];
$stats->rest_json['local_posts']      = $res[1];
$stats->rest_json['local_comments']   = $res[2];
//var_dump($stats);
?>
