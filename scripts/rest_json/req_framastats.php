<?php

/*
 * include by rest_json/mainScript.php
 *
 * Create $stats
 *
 * */

// ################### CONSTANTS ##################
$a_urlsStats = array (
	'degooglisonsInternet' => 'http://degooglisons-internet.org/statistics.json',
	//'framabag' => 'https://framabag.org/statistics.json',
	//'framabee' => 'https://framabee.org/static/statistics.json',
	'framabin' => 'https://framabin.org/statistics.json',
	'framablog' => 'http://framablog.org/statistics.json',
	'framabook' => 'http://www.framabook.org/statistics.json',
	'framacalc' => 'http://framacalc.org/static/statistics.json',
	'framacloud' => 'http://framastats.org/autresStats/framacloud/statistics.json',
	//'framacolibri' => 'https://participer.framasoft.org/statistics.json',
	'framadate' => 'http://framadate.org/statistics.json',
	//'framadvd' => 'http://iso.framadvd.org/statistics.json',
	'framagames' => 'http://framagames.org/statistics.json',
	'framagit' => 'http://git.framasoft.org/statistics.json',
	//'framakey' => 'http://framakey.org/statistics.json',
	'framalab' => 'http://www.framalab.org/statistics.json',
	'framalibre' => 'https://framalibre.org/statistics2.json',
	'framalink' => 'http://www.frama.link/statistics.json',
	'framanews' => 'http://www.framanews.org/statistics.json',
	'framapack' => 'http://framapack.org/statistics.json',
	'framapad' => 'http://framastats.org/autresStats/framapad/statistics.json',
	'framapiaf' => 'http://framastats.org/autresStats/framapiaf/statistics.json',
	'framapic' => 'http://framapic.org/stats.json',
	'framapic_piwik' => 'http://framastats.org/autresStats/framapic/statistics.json',
	'framaslides' => 'https://framastats.org/autresStats/framaslides/statistics.json',
	'framasphere' => 'http://framastats.org/autresStats/framasphere/statistics.json',
	//'framastart' => 'http://framastart.org/statistics.json',
	'framastats' => 'http://framastats.org/statistics.json',
	'peertube' => 'https://instances.joinpeertube.org/api/v1/instances/stats',
	//'framavectoriel' => 'http://framavectoriel.org/statistics.json',
	'framazic' => 'http://framazic.org/statistics.json',
	'framindmap' => 'http://www.framindmap.org/statistics.json',
	'myframa' => 'http://my.framasoft.org/statistics.json',
	'reseauxSociaux' => 'http://framastats.org/autresStats/reseauxSociaux/statistics.json',
);

$a_servicesWithoutPiwik = array ('reseauxSociaux', 'framastats', 'framapic', 'framakey', 'framacolibri', 'peertube', 'framalibre');

$i_howManyServices = count($a_urlsStats);

// ##################### STATS ####################
$stats->rest_json['site'] 			= "All Framasoft services";
$stats->rest_json['embeddedServices']		= $a_urlsStats;
$stats->rest_json['timeUpdateStats']		= date('Y-m-d H:i:s');

$stats->rest_json['nbVisits_all_total']  	= 0;
$stats->rest_json['nbVisits_thisYear_total'] 	= 0;
$stats->rest_json['nbVisits_last365days_total'] = 0;
$stats->rest_json['nbVisits_last180days_total'] = 0;
$stats->rest_json['nbVisits_last90days_total'] 	= 0;
$stats->rest_json['nbVisits_last30days_total']  = 0;
$stats->rest_json['nbVisits_last7days_total']  	= 0;
$stats->rest_json['nbVisits_yesterday_total']  	= 0;
$stats->rest_json['nbVisits_today_total'] 	= 0;

// ################ REQUESTS AND STATS ############

if (!file_exists($tmp_pathFileName)) {
	$util->out("### (scripts/rest_json/req_framastats.php) Temporary stats file has never been created. ", "info");
}

$howManyServicesWithPiwik 	= 0;
$howManyServicesWithoutPiwik	= 0;

$noProblem			= true;
$tmp_nbVisits_all_total		= 0;
$tmp_nbVisits_thisYear_total	= 0;
$tmp_nbVisits_last365days_total	= 0;
$tmp_nbVisits_last180days_total = 0;
$tmp_nbVisits_last90days_total 	= 0;
$tmp_nbVisits_last30days_total 	= 0;
$tmp_nbVisits_last7days_total   = 0;
$tmp_nbVisits_yesterday_total  	= 0;
$tmp_nbVisits_today_total  	= 0;

// Count users for all services
foreach ($a_urlsStats as $service => $url) {
	$arrayStats = json_decode(file_get_contents($url), true);

	if (array_key_exists('pwk',$arrayStats)) {
		$tmp_nbVisits_all_total		+= $arrayStats['pwk']['nbVisits_all'];
		$tmp_nbVisits_thisYear_total	+= $arrayStats['pwk']['nbVisits_thisYear'];
		$tmp_nbVisits_last365days_total	+= $arrayStats['pwk']['nbVisits_last365days'];
		$tmp_nbVisits_last180days_total += $arrayStats['pwk']['nbVisits_last180days'];
		$tmp_nbVisits_last90days_total 	+= $arrayStats['pwk']['nbVisits_last90days'];
		$tmp_nbVisits_last30days_total 	+= $arrayStats['pwk']['nbVisits_last30days'];
		$tmp_nbVisits_last7days_total   += $arrayStats['pwk']['nbVisits_last7days'];
		$tmp_nbVisits_yesterday_total  	+= $arrayStats['pwk']['nbVisits_yesterday'];
		$tmp_nbVisits_today_total  	+= $arrayStats['pwk']['nbVisits_today'];

		$howManyServicesWithPiwik	+= 1;

	}
	elseif (in_array($service,$a_servicesWithoutPiwik)) {
		$util->out("### (scripts/rest_json/req_framastats.php) Stats Rest Framastats : No Piwik stats found for $service !", 'info');
		$howManyServicesWithoutPiwik 	+= 1;
	}
	else {
		$util->out("### (scripts/rest_json/req_framastats.php) Stats Rest Framastats : No Piwik stats found for $service !", 'error');
		$howManyServicesWithoutPiwik 	+= 1;
		$noProblem 			= false;
	};
}

// Record stats only if there was no problem in any services
	$stats->rest_json['nbVisits_all_total']  	= $tmp_nbVisits_all_total;
	$stats->rest_json['nbVisits_thisYear_total'] 	= $tmp_nbVisits_thisYear_total;
	$stats->rest_json['nbVisits_last365days_total']	= $tmp_nbVisits_last365days_total;
	$stats->rest_json['nbVisits_last180days_total']	= $tmp_nbVisits_last180days_total;
	$stats->rest_json['nbVisits_last90days_total']	= $tmp_nbVisits_last90days_total;
	$stats->rest_json['nbVisits_last30days_total']	= $tmp_nbVisits_last30days_total;
	$stats->rest_json['nbVisits_last7days_total']  	= $tmp_nbVisits_last7days_total;
	$stats->rest_json['nbVisits_yesterday_total'] 	= $tmp_nbVisits_yesterday_total;
	$stats->rest_json['nbVisits_today_total'] 	= $tmp_nbVisits_today_total;

if ($noProblem) {
	$util->out("### (scripts/rest_json/req_framastats.php) Services with module Piwik : $howManyServicesWithPiwik.\n### Services without module Piwik : $howManyServicesWithoutPiwik.\n### Total stats : $i_howManyServices services.", 'info');
} else {
	$util->out("### (scripts/rest_json/req_framastats.php) Stats Rest Framastats : Some services don't respond well.", 'error');
}


?>
