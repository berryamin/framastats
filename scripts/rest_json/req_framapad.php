<?php

/*
 * include by rest_json/mainScript.php
 *
 * Create $stats
 *
 * */

// ################### CONSTANTS ##################
$a_urlsStats = array (
	'quotidien'     => 'https://quotidien.framapad.org/stats.json',
	'hebdo'         => 'https://hebdo.framapad.org/stats.json',
	'mensuel'       => 'https://mensuel.framapad.org/stats.json',
	'bimestriel'    => 'https://bimestriel.framapad.org/stats.json',
	'semestriel'    => 'https://semestriel.framapad.org/stats.json',
	'annuel'        => array ('https://annuel.framapad.org/stats.json', 'https://annuel2.framapad.org/stats.json'),
	'eternal'       => 'https://lite6.framapad.org/stats.json',
);
$padsCount_name	= 'padsCount';
$blankPads_name	= 'blankPads';

// ##################### STATS ####################
$stats->rest_json['site']               = "Framapad";
$stats->rest_json['timeUpdateStats']    = date('Y-m-d H:i:s');

// ################ REQUESTS AND STATS ############

if (!file_exists($tmp_pathFileName)) {
	$util->out("### Temporary stats file has never been created. ", "info");
}

$total_pads_living  = 0;
$total_blank_living = 0;

foreach ($a_urlsStats as $category => $url) {
	if (!is_array($url)) {
		$arrayStats         = json_decode(file_get_contents($url), true);
		$total_pads_living  += $arrayStats[$padsCount_name];
		$total_blank_living += $arrayStats[$blankPads_name];
		$stats->rest_json['pluginFramapad'][$category] = $arrayStats;
	} else {
		foreach ($url as $u) {
			$arrayStats         = json_decode(file_get_contents($u), true);
			$total_pads_living  += $arrayStats[$padsCount_name];
			$total_blank_living += $arrayStats[$blankPads_name];

			if (!array_key_exists($category, $stats->rest_json['pluginFramapad'])) {
				$stats->rest_json['pluginFramapad'][$category] = $arrayStats;
			} else {
				$stats->rest_json['pluginFramapad'][$category]['timestamp']     = $arrayStats['timestamp'];
				$stats->rest_json['pluginFramapad'][$category][$padsCount_name] += $arrayStats[$padsCount_name];
				$stats->rest_json['pluginFramapad'][$category][$blankPads_name] += $arrayStats[$blankPads_name];
			}
		}
	}
}

// ##################### STATS ####################
$stats->rest_json['totalPads_living']               = $total_pads_living;
$stats->rest_json['totalBlankPads_living']          = $total_blank_living;
$stats->rest_json['totalBlankPads_living_percent']  = round($total_blank_living/$total_pads_living*100, 2);
?>
