<?php

/*
 * include by rest_json/mainScript.php
 *
 * Create $stats
 *
 * */

// ################### CONSTANTS ##################
$a_urlStat = 'https://framaslides.org/stats';

// ##################### STATS ####################
$stats->rest_json['site'] 			= "Framaslides";
$stats->rest_json['timeUpdateStats']		= date('Y-m-d H:i:s');

// ################ REQUESTS AND STATS ############

if (!file_exists($tmp_pathFileName)) {
	$util->out("### (req_framapiaf) Temporary stats file has never been created. ", "info");
}

// Retrieve stat framaslides
$json = file_get_contents($a_urlStat);
$obj = json_decode($json, true);
//var_dump($obj);

// ##################### STATS ####################
$stats->rest_json['presentations']	= $obj['presentations'];
$stats->rest_json['public_templates']	= $obj['public templates'];
$stats->rest_json['groups']		= $obj['groups'];
$stats->rest_json['enabled_users']	= $obj['enabled users'];
//var_dump($stats);
?>
