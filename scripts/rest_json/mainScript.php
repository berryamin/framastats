<?php

/*
 * include by dispatcher.php
 *
 * Retrieve $stats
 *
 * */

// ################## INCLUDE #####################

// ##############################################

// ################# CONSTANTS #################
$pathReqFramapad        = "req_framapad.php";
$pathReqFramastats      = "req_framastats.php";
$pathReqFramaslides     = "req_framaslides.php";
$pathReqFramapiaf       = "req_framapiaf.php";
$pathReqFramasphere     = "req_framasphere.php";

// ############## MAIN SCRIPT ###################
$util->out("### Stats with rest_json", "info");

// ################### STATS #####################

// add values to $stats which is an instance of Stats
switch ($u_service) {
        case "framapad":
                include($pathReqFramapad);
                break;
        case "framastats":
                include($pathReqFramastats);
                break;
        case "framaslides":
                include($pathReqFramaslides);
                break;
        case "framapiaf":
                include($pathReqFramapiaf);
                break;
        case "framasphere":
                include($pathReqFramasphere);
                break;
        default:
                $stats->site = "default site name";
		$util->out("### (scripts/rest_json/mainScript.php) This service is not initialized for rest_json", "error", true); //exit
                break;
}
?>
