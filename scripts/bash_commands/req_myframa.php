<?php

/*
 * include by pattern/mainScript.php
 *
 * Create $stats
 *
 * */

// ################### CONSTANTS ##################

// ################### REQUESTS ##################

// Calcule le nombre de comptes
$account_nb = exec('cd  /var/www/my.framasoft.org/web/u && find ./ -maxdepth 1 -type d | wc -l');

// ##################### STATS ####################
$stats->bash_commands['timeUpdateStats']	= date('Y-m-d H:i:s');
$stats->bash_commands['account_nb']		= $account_nb;

?>
